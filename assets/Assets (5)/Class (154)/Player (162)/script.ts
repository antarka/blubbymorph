class Player extends EntityBehavior {
    
    name: string = "player";
    spawnScanner : United.Scanner;
 
    private _absorbing:boolean = false;
    private absorb_cooldown:number = 5;

    public getAbsorbing() { return this._absorbing }
    public setAbsorbing(b:boolean) { this._absorbing = b }

    onAwake($) {
        this.maxLife = playerMeta.skins[playerMeta.transformation].maxlife;
        super.onAwake($);
        Game.declare("player", this);
        Game.declare("playerDefaultPosition",this.actor.getPosition());
        let b = this.actor.addBehavior(PlayerControl);
        b.player = this;
        this.type = playerMeta.transformation;
        
        const Spawners : Sup.Actor = Sup.getActor("Spawners");
        if(Spawners)
            this.spawnScanner = new United.Scanner(Sup.getActor("Spawners"),["Spawn_"]);
    }
    
    main($) {
        if(!Game.pause) {
            super.main($);

            if(this.getLife() == 0) {
                if(data.score - 40 > 0) {
                    data.score-= 40;
                }
                else {
                    data.score = 0;
                }
                savedata();
                this.onDie($);
            }
                
        }
    }

    attacked(entity:EntityBehavior, damage:number) {
        if (!this._invulnerable) super.attacked(entity, damage);
    }

    canAbsorb(type:number): boolean {
        return (type <= data.progression && type != this.getType())
    }

    absorb(target:Elemental) {
        if (this.getAbsorbing() || !this.canAbsorb(target.getType()))
            return;
        
        this.setAbsorbing(true);
        
        //TEMPORAIRE
        const type = target.getType();
        const color = target.color;
        const sprite = target.actor.spriteRenderer.getSprite();
        this.renderer.setSpecialAnimation("absorbsion");
        Sup.setTimeout(this.absorb_cooldown * 100, () => {
            this.renderer.changeSprite(sprite);
            Sup.setTimeout(this.absorb_cooldown * 100, () => {
                this.setType(type);
                this.color = color;
                this.setAbsorbing(false);
            });
        });
    }

    setType(type) {
        this.type = type;
        playerMeta.transformation = type;
        this.setMaxLife(playerMeta.skins[playerMeta.transformation].maxlife);
        Sup.getActor("Camera").getChild("HealthBar").getBehavior(HealthBarBehavior).resetMaxHeart(this.getMaxLife());
    }

    onDie($){
        this.setLife(this.getMaxLife());
        this._invulnerable = true;
        Sup.setTimeout(5000, () => {
            this._invulnerable = false;
        });
        this.setType(0);
        this.actor.arcadeBody2D.warpPosition( $.playerDefaultPosition.toVector2() );
        if(this.spawnScanner) {
            this.spawnScanner.forEach("Spawn_",(actor: Sup.Actor) => {
                actor.getBehavior(SpawnerBehavior).reset();
            });
        }
        this.renderer.resetSprite();
        Sup.log("DIE");
    }
}
Sup.registerBehavior(Player);
