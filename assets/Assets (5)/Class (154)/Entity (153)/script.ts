enum entityTypes {
    None,
    Earth,
    Fire,
    Ice,
    Air
}

class EntityBehavior extends United.Behavior {
    
    public maxLife: number = 20;
    public attack_cooldown = 32;

    protected _isDead:boolean = false;
    protected _life: number = 20;

    private moving:boolean = false;
    private current_attack_cooldown = 0;

    //--------------------

    public getMaxLife() { return this.maxLife; }
    public getLife() { return this._life; }
    public addLife(life) { return this.setLife(this.getLife() + life); }
    public setLife(life) {
        if (this._invulnerable) return;
        this._life = Sup.Math.clamp(life, 0, this.getMaxLife());
        if (this.getLife() <= 0) {
            this._isDead = true;
            this.renderer.setSpecialAnimation("death");
        }
        return this;
    }
    public setMaxLife(life) {
        this.maxLife = life - (life % 4);
    }

    name:string = "entity"
    color:string = "0xffffff";
    type:number = entityTypes.None

    renderer:EntityRenderer;

    protected _invulnerable:boolean = false;
    
    onAwake($) {
        this.setMaxLife(this.getMaxLife());
        this.renderer = new EntityRenderer(this.actor);
    }
                          
    onStart($) {
        this.setLife(this.getMaxLife());
    }

    main($) {
        this.current_attack_cooldown -= this.current_attack_cooldown > 0 ? 1 : 0;
    }

    attack(target:EntityBehavior) {
        if (this.onInteract())
            return;

        target.attacked(this,1);
        this.renderer.setAnimation("attack").refresh();
        this.current_attack_cooldown = this.attack_cooldown
    }

    attacked(entity:EntityBehavior, damage:number) {
        
        if (this._invulnerable) return;
        
        const mult = playerMeta.skins[entity.getType()].attackMultiplier[this.getType()];
        damage *= mult;
        this.addLife(-damage);
        const p = Sup.appendScene("Assets/prefabs/Particles/text")[0];
        p.addBehavior(TextParticle, {
            time:64,
            position: this.actor.getPosition(),
            range:0.2,
            text:damage,
            color:new Sup.Color(+entity.color)
        });
    }

    getCurrentAttackCooldown() {
        return this.current_attack_cooldown;
    }

    resetAttackCooldown() {
        this.current_attack_cooldown = this.attack_cooldown;
    }

    getType() {
        return this.type;
    }

    onInteract():boolean {
        return this.current_attack_cooldown > 0;
    }

    onMove() {
        return this.moving;
    }

    onDie($){
        // Game OVER
    }
    
}

class EntityRenderer {
    
    actor:Sup.Actor;
    defaultSprite:Sup.Sprite
    public animation: "idle" | "move" | "attack" = "idle";
    public rotation: "up" | "down" | "left" | "right" = "down";
    
    private animSet:string = "";

    constructor(actor:Sup.Actor) {
        this.actor = actor;
        this.defaultSprite = actor.spriteRenderer.getSprite();
    }

    resetSprite() {
        this.actor.spriteRenderer.setSprite(this.defaultSprite);
        this.refresh();
    }

    changeSprite(sprite:Sup.Sprite) {
        //this.actor.spriteRenderer.setSprite(`Assets/sprites/${name}`);
        this.actor.spriteRenderer.setSprite(sprite);
        this.refresh();
    }

    setAnimation(anim:"idle" | "move" | "attack", rot:"up" | "down" | "left" | "right" = null) {
        this.animation = anim;
        this.rotation = rot || this.rotation;
        this.animSet = `${this.animation}-${this.rotation}`;
        return this;
    }

    setSpecialAnimation(anim:string) {
        this.animSet = anim;
        this.refresh();
    }

    setRotation(rot:"up" | "down" | "left" | "right", anim:"idle" | "move" | "attack" = null) {
        this.rotation = rot;
        this.animation = anim || this.animation;
        this.animSet = `${this.animation}-${this.rotation}`;
        return this;
    }

    setDirection(src:Sup.Math.Vector2, dest:Sup.Math.Vector2) {
        const angle = src.angleTo(dest); 
        this.rotation = angle >= -2.35 && angle <= -0.8 ? "down" : (angle > -0.8 && angle < 0.8 ? "right" : (angle >= 0.8 && angle <= 2.35 ? "up" : "left"));
        this.animSet = `${this.animation}-${this.rotation}`;
        return this;
    }

    refresh() {
        this.actor.spriteRenderer.setAnimation(this.animSet);
    }

}