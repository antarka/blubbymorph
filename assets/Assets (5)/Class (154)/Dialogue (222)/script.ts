class WinDialogue extends United.Behavior {
    
    public WinActive : boolean = false;
    public nextActor : Sup.Actor;
    public textActor: Sup.Actor;
    public phrase: string;
    public dialogue: string[];
    public index: number = 0;
    
    onAwake($) {
        this.nextActor = this.actor.getChild("next");
        this.textActor = this.actor.getChild("texte");
        this.actor.setVisible(false);
        Game.declare("winDialogue",this);
    }
    
    onStart($) {
        const nextButton : United.Button = new United.Button("next");
        nextButton.click = () => {
            const Win : WinDialogue = <WinDialogue>Game.$.winDialogue;
            if(Win.WinActive) {
                Win.next();
            }
        }
    }

    setDialogue(pattern: string[]) {
        if(!this.WinActive) {
            this.actor.setVisible(true);
            this.dialogue = pattern;
            this.textActor.textRenderer.setText(pattern[this.index]);
            this.WinActive = true;
            Game.pause = true;
        }
    }

    close() {
        this.actor.setVisible(false);
        this.index = 0;
        this.WinActive = false;
        Game.pause = false;
    }

    next() {
        if(this.index + 1 == this.dialogue.length) {
            this.close();
        }
        else {
            this.index++;
            this.textActor.textRenderer.setText(this.dialogue[this.index]);
        }
    }

    main($) {
        if(this.WinActive) {
            if(($.controls as playerControl).INTERACT.wasJustPressed()) {
                this.next();
            }
        }
    }
}
Sup.registerBehavior(WinDialogue);