class Elemental extends EntityBehavior {
    
    name:string = "elemental"
    
    infoText:Sup.Actor;

    attackArea: United.Area;
    absorbArea: United.Area;
    detectArea: United.Area;
    
    focus:boolean = false;
    collided:boolean = false;
    destination: Sup.Math.Vector2;
    speed = 0.01;
    
    onAwake($) {
        super.onAwake($);
        this.infoText = this.actor.getChild("infoText");
        this.destination = Utils.generateRandomVector2(1).add(this.actor.getPosition().toVector2());
    }

    onStart($) {
        super.onStart($);
        this.refreshHUD();
        this.attackArea = new United.Area(this.actor, $.player.actor, 0.25);
        this.detectArea = new United.Area(this.actor, $.player.actor, 1.5);
        this.absorbArea = new United.Area(this.actor, $.player.actor, 0.8);
    }

    tryToBeAbsorbed($) : boolean {
        if (this.absorbArea.in() && ($.controls as playerControl).ABSORB.wasJustPressed() && this.detectPlayer()) {
            $.player.absorb(this);
            return true;
        }
        return false;
    }

    /** @return : isAttacking */
    tryAttackOrMove($) : boolean {
        if (this.attackArea.in() && this.detectPlayer()) {
            //If the player is in attack area and he use ATTACT key, attack this entity
            // if (($.controls as playerControl).ATTACK.isDown()) {
            //     $.player.attack(this);
            // }
            this.attack($.player);
            this.actor.arcadeBody2D.setVelocity(0, 0);
            return true;
        }
        this.move($);
        return false;
    }

    main($) {
        if(!Game.pause) {
            super.main($);
            this.collided = Sup.ArcadePhysics2D.collides(this.actor.arcadeBody2D, Sup.ArcadePhysics2D.getAllBodies());

            this.tryToBeAbsorbed($);
            this.tryAttackOrMove($);
        }
        else {
            this.actor.arcadeBody2D.setVelocity({x:0,y:0});
        }
    }

    onMove():boolean {
        return (this.focus || this.destination != null);
    }

    detectPlayer():boolean {
        return playerMeta.transformation != this.getType();
    }

    generateRandomDestination() {
        if (!this.onMove() || this.collided) {
            this.destination = Utils.generateRandomVector2(1).add(this.actor.getPosition().toVector2());
        }
    }

    move($) : void {
        // if (this.onInteract()) {
        //     this.actor.arcadeBody2D.setVelocity(0, 0);
        //     return;
        // }

        this.focus = false;
        const pos : Sup.Math.Vector2 = this.actor.getPosition().toVector2();
        
        if (this.detectArea.in()) {
            this.infoText.setVisible(true);
            if (this.detectPlayer()) {
                this.focus = true;
                this.destination = <Sup.Math.Vector2>$.player.actor.getPosition();   
            }
            this.generateRandomDestination();
        }
        else {
            this.infoText.setVisible(false);
            this.generateRandomDestination();
        }

        if (this.onMove()) {
            if (pos.distanceTo(this.destination) < 0.2) {
                this.destination = null;
                this.actor.arcadeBody2D.setVelocity(0, 0);
                this.renderer.setAnimation("idle").refresh();
            } else {
                //USE A* HERE
                const v : Sup.Math.Vector2 = this.destination.clone().subtract(pos);
                const polar : Polar = new Vector2(v.x, v.y).toPolar();
                polar.rayon = this.speed;
                this.actor.arcadeBody2D.setVelocity(polar.toCarthesian());
                this.renderer.setAnimation("move").setDirection(pos, this.destination).refresh();
            }
        }
    }
    
    refreshHUD() {
        this.infoText.textRenderer.setText(`Life: ${this.getLife()}`);
    }

    attacked(entity:EntityBehavior, damage:number) {
        super.attacked(entity, damage);
        
        if (this.getLife() <= 0) { 
            data.score += 10;
            savedata();
            this.actor.destroy();
        }
        this.destination = entity.actor.getPosition().toVector2();
        this.refreshHUD();
    }

}
Sup.registerBehavior(Elemental);