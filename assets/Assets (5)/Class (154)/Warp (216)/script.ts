class WarpBehavior extends United.AreaBehavior {
    
    public area : number = 2; 
    public targetScene : string = null;
    public enterText: Sup.Actor;
    public warpName:string = "default";
    public state : boolean = false;
    public restrictProgression : number = 0;

    configure($) {
        this.enterText = this.actor.getChild("enterText");
        this.target = $.player.actor;
        this.area_length = this.area;
        if(data.progression < this.restrictProgression) {
            this.enterText.textRenderer.setText("Please unlock precedent dungeon!");
        }
    }
    
    in($) {
        if( ($.controls as playerControl).INTERACT.wasJustPressed() && data.progression >= this.restrictProgression) {
            $.warp(this.targetScene, this.warpName);
        }
        if(!this.state) {
            this.state = true;
            this.enterText.setVisible(true);
        }
    }

    out($) {
        if(this.state) {
            this.state = false;
            this.enterText.setVisible(false);
        }
    }
    
}
Sup.registerBehavior(WarpBehavior);
