class SpawnerBehavior extends United.Behavior {
    
    public prefab: string;
    public range: number = 0.5;
    public spawnLimit = 2;
    public cooldown: number = 10;
    public progressRequired: number = 0;

    private DT : United.dt;
    private entities:Sup.Actor[] = [];

    onAwake($) {
        this.DT = new United.dt({dt:this.cooldown * 60});
        if (this.canSpawn()) {
            this.generateMob(Sup.Math.Random.integer(0, this.spawnLimit));
        }
    }

    reset() {
        // for (let i = 0; i < this.entities.length; i++) {
        //     this.entities[i].destroy();
        // }
        this.entities.pop
    }

    canSpawn():boolean {
        return data.progression >= this.progressRequired;
    }

    generateMob(n:number) {
        const mob : Sup.Actor = Sup.appendScene(this.prefab, this.actor.getParent())[0];
        mob.arcadeBody2D.warpPosition(this.actor.getLocalPosition().add(Utils.generateRandomVector2(this.range).toVector3()));   
        this.entities.push(mob);
    }

    main($) {
        if(!Game.pause && this.canSpawn()) {
            if(this.DT.walk()) {
                if (this.isUnderLimit()) {
                    this.generateMob(1);
                }
                this.DT.reset();
            }
        }
    }

    isUnderLimit():boolean {
        for (let i = 0; i < this.entities.length; i++) {
            if (this.entities[i].isDestroyed()) {
                this.entities.splice(i, 1);
            }
        }
        return this.entities.length < this.spawnLimit
    }
}
Sup.registerBehavior(SpawnerBehavior);