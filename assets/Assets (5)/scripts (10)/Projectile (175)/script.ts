class ProjectilBehavior extends United.Behavior {
    
    public static count : number = 0;
    public static get MAXCOUNT() : number {return 5};

    private vector : Vector2;
    private emiter : EntityBehavior;
    
    speed  : number = 0.04;
    life   : number = 1 * 60
    damage : number = 1;

    sound_launch:string = "Assets/musiques/Spell";
    sound_fail:string = "Assets/musiques/SpellFail";
    
    private immunity = 4;
    private _sprite:Sup.Sprite;
    private _color:Sup.Color;

    init( entity : EntityBehavior, angle:number ) {
        
        Sup.Audio.playSound(this.sound_launch, 0.2);
        
        this._sprite = this.actor.spriteRenderer.getSprite();
        this._color = this.actor.spriteRenderer.getColor();
        
        ProjectilBehavior.count++;
        this.actor.arcadeBody2D.setEnabled(false);
        const pos = entity.actor.getPosition();
        this.warpPosition(pos);
        let orient = new Sup.Math.Quaternion();
        orient.setFromYawPitchRoll(0,0,angle);
        this.actor.setOrientation(orient);
        
        this.emiter = entity;
        this.vector = new Vector2(1,1);
        this.vector._length = this.speed;
        this.vector._angle = angle;
        this.actor.arcadeBody2D.setVelocity(this.vector);
    }

    onDestroy() {
        ProjectilBehavior.count--;
        let explosion = Sup.appendScene("Assets/prefabs/Particles/impact")[0];
        explosion.spriteRenderer.setSprite(this._sprite);
        explosion.spriteRenderer.setColor(this._color);
        explosion.spriteRenderer.setAnimation("explosion");
        explosion.setPosition(this.actor.getPosition());
    }

    warpPosition(pos:Sup.Math.XYZ) {
        this.actor.arcadeBody2D.warpPosition(pos);
        this.actor.setZ(pos.z);
    }

    main($) {
        if(!Game.pause) {
            if (this.immunity >= 0) {
                this.actor.move(this.vector);
                if (this.immunity == 0) {
                    this.warpPosition(this.actor.getPosition());
                    this.actor.arcadeBody2D.setEnabled(true);
                }
                this.immunity--;
                return;
            }
            const bodies = Sup.ArcadePhysics2D.getAllBodies();
            for (let i = 0; i < bodies.length; i++) {
                const collide = Sup.ArcadePhysics2D.collides(this.actor.arcadeBody2D, bodies[i]);
                if (collide) {
                    if (bodies[i].actor == this.emiter.actor) {
                        continue;
                    }
                    let e = bodies[i].actor.getBehavior(EntityBehavior);
                    if (e != null) {
                        e.attacked(this.emiter, this.damage);
                    }
                    Sup.Audio.playSound(this.sound_fail, 0.08);
                    this.actor.destroy();
                }
            }

            if(this.life <= 0 ){
                this.actor.destroy();
            }
            this.life--;
        }
    }
}
Sup.registerBehavior(ProjectilBehavior);
