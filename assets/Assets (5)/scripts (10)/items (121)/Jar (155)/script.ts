type JarState = "whole" | "broken";

class JarBehavior extends U.AreaBehavior {
    
    area: number = 0.3;
    sound:string = "Assets/musiques/GlassBreak"

    configure($) {
        this.target = $.player.actor;
        this.area_length  = this.area;
    }

    in($) {
        if(($.controls as playerControl).INTERACT.wasJustPressed() && this.state === "whole"){
            this.state = "broken";
            this.actor.arcadeBody2D.destroy();
            Sup.Audio.playSound(this.sound, 0.15);
            data.healpotion++;
            savedata();
        }
    }

    get state() {
        return this.actor.spriteRenderer.getAnimation() as JarState;
    }
    set state(newState: JarState) {
        this.actor.spriteRenderer.setAnimation(newState, false);
    }
}
Sup.registerBehavior(JarBehavior);
