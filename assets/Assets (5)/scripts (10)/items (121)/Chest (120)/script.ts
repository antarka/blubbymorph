type ChestState = "open" | "lock";

class ChestBehavior extends U.AreaBehavior {
    
    configure($) {
        this.target = $.player.actor;
        this.area  = 0.3;
    }

    in($) {
        if(($.controls as playerControl).INTERACT.wasJustPressed() && this.state === "lock"){
            this.state = "open";
            data.healpotion++;
            savedata();
        }
    }

    get state() {
        return this.actor.spriteRenderer.getAnimation() as ChestState;
    }
    
    set state(newState: ChestState) {
        this.actor.spriteRenderer.setAnimation(newState, false);
    }
}
Sup.registerBehavior(ChestBehavior);
