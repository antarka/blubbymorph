class ArenaDialogueFirstBehavior extends United.Behavior {
    
    onStart($) {
        if(!firstDialogue) {
            firstDialogue=true;
            $.winDialogue.setDialogue(
                [
                    "Welcome to blubby\nmorph little kid!",
                    "Interact with object\nwith the key 'E' or 'SPACE'",
                    "When you get\nhealthpotion press the key\n'P' to use one potion."
                ]
            );
        }
    }
    
}
Sup.registerBehavior(ArenaDialogueFirstBehavior);
