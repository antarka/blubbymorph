class RocherBehavior extends United.Behavior {
    
    public progressionRestrict : number = 0;
    detectArea: United.Area;
    infoText:Sup.Actor;

    onAwake($) {
        if(data.progression >= this.progressionRestrict) {
            this.actor.arcadeBody2D.destroy();
            this.actor.destroy();
            delete this;
        }
        this.infoText = this.actor.getChild("text");
        this.detectArea = new United.Area(this.actor, $.player.actor, 1);
    }

    activeInfoText(state:boolean) {
        if (this.infoText) {
            this.infoText.setVisible(state);
        }
    }

    main($) {
        this.activeInfoText(false);
        if(!Game.pause) {
            if (this.detectArea.in()) {
                this.activeInfoText(true);
            }
        }
    }
}
Sup.registerBehavior(RocherBehavior);
