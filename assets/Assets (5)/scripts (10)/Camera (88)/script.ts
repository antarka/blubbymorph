class CameraBehavior extends United.Behavior {
    
    private speed : number = 0.065;
    private max_speed : number = 30;
    private height : number = 0;
    private lookDT : United.dt;

    awake() {
        Game.declare("camera",this.actor);
    }

    lootAt(target: Sup.Actor,stayTime: number) : void {
        
    }

    main($) {
        let player : Player = $.player;
        const radians : number = Sup.Math.toRadians(player.actor.getEulerY());
        const dy : number = (player.actor.getY() + this.height) - this.actor.getY();
        const vy : number = dy * this.speed;
        const dx : number = (player.actor.getX() + this.height) - this.actor.getX();
        const vx : number = dx * this.speed;
        this.actor.move( (vx > this.max_speed || vx < -this.max_speed) ? (vx < 1 ? -this.max_speed : this.max_speed) : vx , (vy > this.max_speed || vy < -this.max_speed) ? (vy < 1 ? -this.max_speed : this.max_speed) : vy , 0 );
    }
}
Sup.registerBehavior(CameraBehavior);
