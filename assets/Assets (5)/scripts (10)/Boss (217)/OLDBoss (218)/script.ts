class OLDBossBehavior extends EntityBehavior {
    
    // Boss properties!
    public speed : number = 0.022;
    public damage : number = 2;

    public attackRangeMin : number = 1;
    private attackRangeMax : number = 1.3;
    private needToMove : boolean = false;
    private canAttack : boolean = false;
    public rangeStartBattle : number = 0.8;
    public battleStarted : boolean = false;

    public attackSpeed : number = 1*60;
    public timerAttackSpeed : number = 0;
    public durationStateUseAbility : number = 5 * 60;

    private attackArea;

    _helpFirst : boolean = true;

    onAwake($){
        super.onAwake($);
        this.attackArea = new United.Area(this.actor, $.player.actor, 1);
    }

    onDestroy() {
        Sup.appendScene("Assets/prefabs/WarpBack", Sup.getActor("Warps"))[0].setPosition(this.actor.getPosition());
        //PRINT ABSOBSION INFORMATION IF NEW LEVEL == 1
    }

    printHelpDialogue($) {
        if (data.progression > 0 || !this._helpFirst) return;
        $.winDialogue.setDialogue(
            [
                "Blubby! Your power grows!\nAbsorbs the power of your enemy.\nUse the right mouse click. NOW !!!"
            ]
        );
        this._helpFirst = false;
    }

    deathPhase($) {
        if (this.getLife() <= 0) {
            this._invulnerable = true;
            this.printHelpDialogue($);
            if (this.attackArea.in() && ($.controls as playerControl).ABSORB.wasJustPressed()) {
                data.progression += data.progression < this.getType() ? 1 : 0;
                $.player.absorb(this);
                this._invulnerable = false;
                savedata();
            }
        }
        this.onDie($);
    }

    attackPhase():boolean {
        return false;
    }

    escapePhase():boolean {
        return false;
    }

    battlePhase($) {
        //this.attack($.player);
        const player : Player = $.player;
                let distance = this.distanceFromPlayer($);
                /*Sup.log("distance : " + distance);
                Sup.log("this.attackRange : " + this.attackRangeMin);
                Sup.log("distance > this.attackRange : " + (distance <= this.attackRangeMin));*/
                if(this.needToMove && distance > this.attackRangeMin ){
                    let pos = this.actor.getPosition().toVector2();
                    let playerPos = player.actor.getPosition().toVector2()
                    const v : Sup.Math.Vector2 = playerPos.clone().subtract(pos);
                    const polar : Polar = new Vector2(v.x, v.y).toPolar();
                    polar.rayon = this.speed;
                    this.actor.arcadeBody2D.setVelocity(polar.toCarthesian());
                    this.renderer.setAnimation("move")//.setDirection(pos, playerPos).refresh();

                }
                else{
                    if(distance < this.attackRangeMax){
                        this.needToMove = false;
                        this.actor.arcadeBody2D.setVelocity(0, 0);
                        this.renderer.setAnimation("idle");

                        if(this.timerAttackSpeed >= this.attackSpeed){
                            this.timerAttackSpeed = 0;
                            this.canAttack = true;
                        }
                        else{
                            this.timerAttackSpeed++;
                            this.canAttack = false;
                        }
                        if(this.canAttack){
                            // lancer l'attaque

                        }
                    }
                    else{
                        this.needToMove = true;
                    }
                }
                let pos = this.actor.getPosition().toVector2();
                let playerPos = player.actor.getPosition().toVector2();
                this.renderer.setDirection(pos, playerPos).refresh();
                //Sup.log(`life : ${this.life}`);
    }

    main($){
        if(!Game.pause) {
            super.main($);
            
            this.deathPhase($);

            if (this._isDead) {
                return;
            }
            
            if(this.battleStarted){
                this.battlePhase($);
            }
            else{
                this.playerStartBattle($);
            }
        }
    }

    playerStartBattle($){
        // check distance between player and boss to start the battle;
        let distance = this.distanceFromPlayer($);
        if(distance <= this.rangeStartBattle){
            this.battleStarted = true;
            Sup.log("Battle started with the boss !");
        }
    }

    distanceFromPlayer($): number{
        let playerPos = $.player.actor.getPosition().toVector2();
        let position = this.actor.getPosition().toVector2();
        return playerPos.distanceTo(position);
    }
    
    directionFromPlayer():Sup.Math.Vector2{
        return;
    }

    moveToPlayer(){
        
    }

    onDie($){
        if (this.getLife() <= 0 && !this._invulnerable)
            this.actor.destroy();
    }

}
Sup.registerBehavior(OLDBossBehavior);
