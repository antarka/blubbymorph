class BossBehavior extends EntityBehavior {
    
    // Boss properties!
    public speed : number = 0.01;
    public damage : number = 2;
    public rangeStartBattle : number = 2.5;

    meleeAttackRange:number = 0.45;
    distanceAttackRange:number = 2.2;

    private _meleeArea;
    private _rangeArea;
    private _helpFirst : boolean = true;
    private _battleStarted: boolean = false;

    onAwake($){
        super.onAwake($);
        this._meleeArea = new United.Area(this.actor, $.player.actor, this.meleeAttackRange);
        this._rangeArea = new United.Area(this.actor, $.player.actor, this.distanceAttackRange);
    }

    onDestroy() {
        Sup.appendScene("Assets/prefabs/WarpBack", Sup.getActor("Warps"))[0].setPosition(this.actor.getPosition());
    }

    printHelpDialogue($) {
        if (data.progression > 0 || !this._helpFirst) return;
        $.winDialogue.setDialogue(
            [
                "Blubby! Your power grows!\nAbsorbs the power of your enemy.\nUse the right mouse click. NOW !!!"
            ]
        );
        this._helpFirst = false;
    }

    deathPhase($) {
        this.actor.arcadeBody2D.setVelocity({x:0,y:0});
        if (this.getLife() <= 0) {
            this._invulnerable = true;
            this.printHelpDialogue($);
            if (this._meleeArea.in() && ($.controls as playerControl).ABSORB.wasJustPressed()) {
                data.progression += data.progression < this.getType() ? 1 : 0;
                $.player.absorb(this);
                this._invulnerable = false;
                savedata();
            }
        }
        this.onDie($);
    }

    attackPhase($):boolean {
        if (!this.meleeAttack($)) {
            if (!this.rangeAttack($) && this._meleeArea.out()) {
                this.moveToPlayer($);
            } else {
                this.actor.arcadeBody2D.setVelocity(0, 0);
            }
        }
        const pos = this.actor.getPosition().toVector2();
        let playerPos = $.player.actor.getPosition();
        this.renderer.setDirection(pos, playerPos).refresh();
        return true;
    }

    escapePhase():boolean {
        return false;
    }

    meleeAttack($): boolean {
        if (this._meleeArea.in() && this.getCurrentAttackCooldown() == 0) {
            this.attack($.player);
            this.resetAttackCooldown();
            return true;
        }
        return false;
    }

    main($){
        if(!Game.pause) {
            super.main($);
            this.deathPhase($);
            if (this._isDead) return;
            if (this._battleStarted) {
                Sup.ArcadePhysics2D.collides(this.actor.arcadeBody2D,Sup.ArcadePhysics2D.getAllBodies());
                this.attackPhase($);
            }
            else {
                this.playerStartBattle($);
            }
        }
        else {
            this.actor.arcadeBody2D.setVelocity({x:0,y:0});
        }
    }

    playerStartBattle($) {
        if(this.distanceFromPlayer($) <= this.rangeStartBattle) this._battleStarted = true;
    }

    distanceFromPlayer($): number{
        let playerPos = $.player.actor.getPosition().toVector2();
        let position = this.actor.getPosition().toVector2();
        return playerPos.distanceTo(position);
    }

    getOrientation(entity: EntityBehavior):Polar {
        const pos : Sup.Math.Vector3 = this.actor.getPosition();
        const entityPos : Sup.Math.Vector3 = entity.actor.getPosition();
        return new Vector2(entityPos.x - pos.x, entityPos.y - pos.y).toPolar();
    }

    moveToPlayer($) {    
        const polar = this.getOrientation($.player);
        polar.rayon = this.speed;
        this.renderer.setAnimation("move");
        this.actor.arcadeBody2D.setVelocity(polar.toCarthesian());
    }

    rangeAttack($): boolean {
        if (this._rangeArea.in() && this.getCurrentAttackCooldown() == 0) {
            let projectile : Sup.Actor = Sup.appendScene(playerMeta.skins[this.getType()].fire)[0];
            let behavior = projectile.getBehavior(ProjectilBehavior);
            const polar = this.getOrientation($.player);
            behavior.init(this, polar.theta);
            this.resetAttackCooldown();
            return true;
        }
        return false;
    }


    onDie($){
        if (this.getLife() <= 0 && !this._invulnerable) {
            data.score+= 30;
            savedata();
            this.actor.destroy();
        }
    }

}
Sup.registerBehavior(BossBehavior);
