class HGBehavior extends United.Behavior {
    
    fade : United.Fade;
    hud : United.HUD;
    blubby: Sup.Actor;

    awake() {
        HorsGame.init();
        this.blubby = Sup.getActor("Blubby");
        this.blubby.spriteRenderer.setAnimation("move-down",true);
        
        let fade : Sup.Actor = Sup.getActor("Fade");
        
        this.fade = new United.Fade({
            fadeActor: fade,
            defaultState : true,
            autoStart: true
        });
        HorsGame.declare('fade',this.fade);
        
        this.hud = new United.HUD({
            camera: this.actor.camera,
            defaultContainer: "containers_accueil"
        });
        HorsGame.declare("HUD",this.hud);
        
        HorsGame.declare('enterGame',() => {
            const fade : United.Fade = HorsGame.$.fade as United.Fade;
            if(!fade._start) {
                fade._callback = function() {
                    const callback : United.callback<void> = () => {
                        const from = Sup.getActor("Warps");
                        let warp = from.getChild("default");
                        Game.$.player.actor.arcadeBody2D.warpPosition(warp.getPosition());
                    }
                    HorsGame.loadScene("Assets/scenes/Game",callback);
                }
                fade.state = false;
            }
        });
        
    }

    onStart($) {
        let play_button: United.Button = new United.Button("button_play");
        play_button.click = () => {
            if(!$.fade._start) {
                this.blubby.spriteRenderer.setAnimation("absorbsion",false);
                Sup.setTimeout(600,() => {
                    this.blubby.destroy();
                });
                HorsGame.$.enterGame();
            }
        }
        
        play_button.hover = () => {
            play_button.sprite("Assets/sprites/Hud/blubby_button_hover");
        }
    }

    main($) {
        HorsGame.update();
        this.fade.update();
        this.hud.update();
        if(Sup.Input.wasKeyJustPressed("SPACE")) {
            this.blubby.spriteRenderer.setAnimation("absorbsion",false);
            HorsGame.$.enterGame();
        }
    }
    
}
Sup.registerBehavior(HGBehavior);
