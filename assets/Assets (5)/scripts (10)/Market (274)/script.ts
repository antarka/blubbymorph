class MarketBehavior extends United.AreaBehavior {
    
    private marketIsOpen :  boolean = false;
    
    configure($) {
        this.target = $.player.actor;
        this.area_length = 0.8;
        
        const regenBTN : United.Button  = new United.Button("button_market_regen");
        const bulletBTN : United.Button = new United.Button("button_market_bullet"); 
        const attackSpeed : United.Button = new United.Button("button_market_speedAttack");
        
        if(data.shootDivided == 15) {
            Sup.getActor("button_market_speedAttack").setVisible(false);
        }
        
        regenBTN.click = () => {
            if(data.score >= 100) {
                data.score -= 100;
                data.regenLife_multiplier+= 1;
                Sup.Audio.playSound("Assets/musiques/powerup", 0.2);
                savedata();
            }
            else {
                Sup.Audio.playSound("Assets/musiques/alert", 0.2);
            }
        }
        
        bulletBTN.click = () => {
            if(data.score >= 100) {
                data.score -= 100;
                data.max_attack_bonus+= 2;
                Sup.Audio.playSound("Assets/musiques/powerup", 0.2);
                savedata();
            }
            else {
                Sup.Audio.playSound("Assets/musiques/alert", 0.2);
            }
        }
        
        attackSpeed.click = () => {
            if(data.score >= 100 && data.shootDivided < 15) {
                data.score -= 100;
                data.shootDivided+= 5;
                Sup.Audio.playSound("Assets/musiques/powerup", 0.2);
                savedata();
            }
            else {
                Sup.Audio.playSound("Assets/musiques/alert", 0.2);
            }
        }
    }
    
    in($) {
        const controls : playerControl = <playerControl>$.controls;
        if(controls.INTERACT.wasJustPressed() && !this.marketIsOpen) {
            Sup.log("Open the market!!");
            this.marketIsOpen = true;
            $.HUD.setContainer("containers_market");
        }
        else if(controls.INTERACT.wasJustPressed() && this.marketIsOpen) {
            this.marketIsOpen = false;
            $.HUD.setContainer("containers_accueil");
        }
    }

    out($) {
        if(this.marketIsOpen) {
            this.marketIsOpen = false;
            $.HUD.setContainer("containers_accueil");
        }
    }
    
}
Sup.registerBehavior(MarketBehavior);
