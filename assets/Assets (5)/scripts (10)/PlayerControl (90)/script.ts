class PlayerControl extends United.Behavior {

    private controls : playerControl = new playerControl();
    player: Player;

    private spriteRndr : Sup.SpriteRenderer = this.actor.spriteRenderer; 
    private spriteSizeUnit : {width:number, height:number} = {width:0, height:0};
    private spritePixelToUnit : number;
    private tileMap : Sup.TileMap;
    private mapPixelToUnit : number;   

    private scaleOrtho    : number; 
    private screenSize    : {x :number, y :number};
    private mousePos      : {x :number, y :number};

    private checkLeft : number = 0;
    private checkTop : number = 0;
    private checkRight : number = 0;

    public speed = 0.015;
    public autoShootDelay = (25 - data.shootDivided);
    public curentDelay = 0;

    awake() {
        Game.declare("controls",this.controls);
        
        this.actor.spriteRenderer.setSprite(playerMeta.skins[playerMeta.transformation].sprite);
        
        let spriteActor = this.spriteRndr.getSprite();
        let spriteSize = spriteActor.getGridSize();
        let map = Sup.getActor("Map");
        let mapRndr = map.tileMapRenderer;
        this.tileMap = mapRndr.getTileMap();
        this.mapPixelToUnit = this.tileMap.getPixelsPerUnit();
        
        this.spritePixelToUnit = spriteActor.getPixelsPerUnit();
        this.spriteSizeUnit.width = spriteSize.width / this.spritePixelToUnit;
        this.spriteSizeUnit.height = spriteSize.height / this.spritePixelToUnit;
        
        let position = this.actor.getPosition();
        this.checkLeft = position.x - this.spriteSizeUnit.width;
        this.checkRight = position.x + this.spriteSizeUnit.width;
        this.checkTop = position.y + this.spriteSizeUnit.height;
    }

    main($) {
        if(!Game.pause) {
            let player : Player = $.player;

            if (this.player.getAbsorbing()) {
                this.actor.arcadeBody2D.setVelocity(0, 0);
                return;
            }

            Sup.ArcadePhysics2D.collides(this.actor.arcadeBody2D,Sup.ArcadePhysics2D.getAllBodies());

            this.move($);
            this.attack($);

            this.screenSize = Sup.Input.getScreenSize();
            let camera = $.camera.camera;
            this.scaleOrtho = camera.getOrthographicScale();

            let position = this.actor.getPosition();
            this.checkLeft = position.x - this.spriteSizeUnit.width;
            this.checkRight = position.x + this.spriteSizeUnit.width;
            this.checkTop = position.y + this.spriteSizeUnit.height; 

            if(this.controls.CONSUME.wasJustPressed() &&  player.getMaxLife() != player.getLife() && data.healpotion > 0) {
                data.healpotion--;
                player.addLife(4);
            }
        }
        else {
            this.actor.arcadeBody2D.setVelocity({x:0,y:0});
        }
    }

    tryAndCanAttack() {
        return this.controls.ATTACK.isDown() && ProjectilBehavior.count <= (ProjectilBehavior.MAXCOUNT + data.max_attack_bonus) && this.curentDelay <= 0;
    }

    attack($) {
        if(this.tryAndCanAttack()) {
            // créer l'acteur et mettre le rendu de sprite dessus
            let fireball : Sup.Actor = Sup.appendScene(playerMeta.skins[playerMeta.transformation].fire)[0];
            let behavior = fireball.getBehavior(ProjectilBehavior);
            
            behavior.init($.player, this.getAngleFromMouse($));
            /*fireball.setEulerZ(this.getAngleFromMouse($));
            fireball.setPosition($.player.actor.getPosition());
            fireball.addBehavior(ProjectilBehavior);*/
            this.curentDelay = this.autoShootDelay;
        }
        this.curentDelay --;
    }

    move($: any) {
        // const Velocity : Sup.Math.Vector2 = this.actor.arcadeBody2D.getVelocity();
        const velocity : Vector2 = new Vector2(0,0);
        velocity._length = this.speed * playerMeta.skins[$.player.getType()].speedMultiplier;
        
        let isMoving = false;
        
        if (this.controls.UP.isDown() && this.controls.LEFT.isDown()) {
            velocity._angle = 3 * Math.PI / 4;
            this.player.renderer.setAnimation("move", "up");
        }
        else if (this.controls.UP.isDown() && this.controls.RIGHT.isDown()) {
            velocity._angle = Math.PI / 4;
            this.player.renderer.setAnimation("move", "up");
        }
        else if (this.controls.DOWN.isDown() && this.controls.RIGHT.isDown()) {
            velocity._angle = - Math.PI / 4;
            this.player.renderer.setAnimation("move", "down");
        }
       else  if (this.controls.DOWN.isDown() && this.controls.LEFT.isDown()) {
            velocity._angle = -3 * Math.PI / 4;
            this.player.renderer.setAnimation("move", "down");
        }
        else if(this.controls.UP.isDown()) {
            velocity._angle = Math.PI / 2;
            this.player.renderer.setAnimation("move", "up");
        }
        else if(this.controls.DOWN.isDown()) {
            velocity._angle = - Math.PI / 2;
            this.player.renderer.setAnimation("move", "down");
        }
        else if(this.controls.RIGHT.isDown()) {
            velocity._angle = 0;
            this.player.renderer.setAnimation("move", "right");
        }
        else if(this.controls.LEFT.isDown()) {
            velocity._angle = Math.PI;
            this.player.renderer.setAnimation("move", "left");
        }
        else {
            velocity._length = 0;
            this.player.renderer.setAnimation("idle");
        }

        if (this.tryAndCanAttack()) {
            this.player.renderer.setAnimation('attack');
        }
        
        this.player.renderer.refresh();
        this.actor.arcadeBody2D.setVelocity(velocity);
    }
    
    getAngleFromMouse($) : number {
        const playerPos : Sup.Math.Vector2 = $.player.actor.getPosition().toVector2();
        let mousePos : Sup.Math.Vector2 = Sup.Input.getMousePosition();
        
        // mise en coordonnes carthesienne de la position de la souris par rapport au ratio de la camera
        mousePos.y =  mousePos.y / $.camera.camera.getWidthToHeightRatio();
        // position souris par rapport au joueur
        mousePos = mousePos.add(playerPos);
        
        let direction : any = mousePos.subtract(playerPos);
        direction = new Vector2(direction.x, direction.y).toPolar();
        
        return direction.theta;
    }
}
Sup.registerBehavior(PlayerControl);