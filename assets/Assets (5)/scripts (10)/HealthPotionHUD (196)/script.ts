class HealthPotionHUDBehavior extends United.Behavior {
    
    textRenderer: Sup.TextRenderer;
    
    onAwake($) {
        this.textRenderer = this.actor.getChild("quantity").textRenderer;
    }

    main($) {
        this.textRenderer.setText(`${data.healpotion}`);
    }
}
Sup.registerBehavior(HealthPotionHUDBehavior);
