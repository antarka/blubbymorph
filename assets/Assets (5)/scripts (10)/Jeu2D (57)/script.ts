class GameBehavior extends United.Behavior {

    fade : United.Fade;
    hud : United.HUD;

    awake() {
        Game.init();
        Game.declare('jeu2D', this);
        
        let fade : Sup.Actor = Sup.getActor("Fade");
        
        this.fade = new United.Fade({
            fadeActor: fade,
            defaultState : true,
            autoStart: true
        });
        Game.declare('fade',this.fade);
        
        this.hud = new United.HUD({
            camera: Sup.getActor("Camera").camera,
            defaultContainer: "containers_accueil"
        });
        Game.declare("HUD",this.hud);
        
        Game.declare('warp',(scene:string, name:string = "default") => {
            const fade : United.Fade = Game.$.fade as United.Fade;
            if(!fade._start) {
                fade._callback = function() {
                    const callback : United.callback<void> = () => {
                        const from = Sup.getActor("Warps");
                        let warp = from.getChild(name) || from.getChild("default");
                        Game.$.player.actor.arcadeBody2D.warpPosition(warp.getPosition());
                    }
                    if (scene) {
                        Game.loadScene(scene, callback);
                    } else {
                        callback();
                        fade._callback = undefined;
                        fade.state = true;
                    }
                }
                fade.state = false;
            }
        });
    }


    main($) {
        Game.update();
        this.fade.update();
        this.hud.update();
    }
        
}
Sup.registerBehavior(GameBehavior);