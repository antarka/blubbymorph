class ScoreBehavior extends Sup.Behavior {
    
    private save: number;
    awake() {
        this.save = data.score;
        this.actor.textRenderer.setText(`score : ${data.score}`);
    }

    update() {
        if(this.save != data.score) {
            this.save = data.score;
            this.actor.textRenderer.setText(`score : ${data.score}`);
        }
    }
}
Sup.registerBehavior(ScoreBehavior);
