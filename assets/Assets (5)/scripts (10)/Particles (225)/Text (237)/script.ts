class TextParticle extends Sup.Behavior {
    
    time:number = 1;
    range:number = 1;
    text:string = "X";
    color:Sup.Color = new Sup.Color(0xffffff);
    position:Sup.Math.Vector3 = null;
    
    awake() {
        const text = this.actor.textRenderer.setText(this.text).setColor(this.color);
        this.position = this.position || this.actor.getPosition();
    }
    
    start() {
        this.actor.setPosition(this.position.add(Sup.Math.Random.float(-this.range, this.range), Sup.Math.Random.float(-this.range, this.range), 0));
    }

    update() {
        if (this.time <= 0) {
            this.actor.destroy();
        }
        this.time--; 
    }
}
Sup.registerBehavior(TextParticle);
