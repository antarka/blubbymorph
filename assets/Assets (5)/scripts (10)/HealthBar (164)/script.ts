class HealthBarBehavior extends United.Behavior {
    
    heartActors: Sup.Actor[];
    parent: Sup.Actor = Sup.getActor("Camera").getChild("HealthBar");
    naturalRegenTime: number = 5; // Seconds to regen a quarter heart

    private dt: U.dt;
    private _lock:boolean = true;

    onStart($) {
        this.setMaxHeart(($.player as Player).getMaxLife());
        
        // Natural regen
        this.dt = new U.dt({
            dt: U.fps * (this.naturalRegenTime / data.regenLife_multiplier ),
            loop: true,
            auto: true
        });
    }

    resetMaxHeart(maxLife) {
        this.cleanHeartActors();
        this.setMaxHeart(maxLife);
    }

    setMaxHeart(maxLife) {
        this.heartActors = [];
        if (maxLife % 4 != 0) throw new Error("Max health must be a multiple of 4 !");

        for(let i = 0; i < maxLife / 4; ++i) {
            const newHeart: Sup.Actor = Sup.appendScene("Assets/prefabs/heart", this.parent)[0];
            newHeart.setLocalPosition(i / 5, 0, 0);
            this.heartActors.push(newHeart);
        }
        this._lock = false;        
    }

    cleanHeartActors() {
        this._lock = true;
        for(let i = 0; i < this.heartActors.length; ++i) {
            this.heartActors[i].destroy();
        }
        this.heartActors.pop();
    }

    onDestroy() {
        // this.cleanHeartActors();
    }

    main ($) {
        if(!Game.pause) {
            if (this._lock) { return; }
            if(this.dt.walk()) ($.player as Player).addLife(1);

            const health: number = ($.player as Player).getLife();

            let fullHearts: number = Math.floor(health / 4); // Full hearts
            let lolPauvre: number = (health / 4 - fullHearts) / 0.25; // Quarters of next heart

            // for (let i = 0; i < this.heartActors.length; i++) {
            //     if(fullHearts > 0){
            //         this.heartActors[i].spriteRenderer.setAnimation("4");
            //         --fullHearts;
            //     }else if(lolPauvre > 0 && lolPauvre % 1 == 0 && lolPauvre <= 4){
            //         this.heartActors[i].spriteRenderer.setAnimation(`${lolPauvre}`);
            //         lolPauvre = 0;
            //     }else{
            //        this.heartActors[i].spriteRenderer.setAnimation("0"); 
            //     }
            // }
            this.heartActors.forEach((heart: Sup.Actor, index: number) => {
                if(fullHearts > 0){
                    heart.spriteRenderer.setAnimation("4");
                    --fullHearts;
                }else if(lolPauvre > 0 && lolPauvre % 1 == 0 && lolPauvre <= 4){
                    heart.spriteRenderer.setAnimation(`${lolPauvre}`);
                    lolPauvre = 0;
                }else{
                   heart.spriteRenderer.setAnimation("0"); 
                }
            });
        }
    }
}
Sup.registerBehavior(HealthBarBehavior);
