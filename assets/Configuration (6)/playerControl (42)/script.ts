/**
étendre la class Controls permet de mettre ses controles de bases en dur, et de pouvoir y faire appele simplement dans le code
aidé par l'autocomplétion de Superpowers

Par contre si vous utilisez les fonctions hérité de Controls permettant d'éditer les Actions
particulièrement renameAction et addAction vous ne pourrez pas faire appele à elles dans le code via
control.ACTIONAJOUTE mais vous devrez utiliser
control["ACTIONAJOUTE"]

--- NotaBene : ---
/!\ Tout les noms d'actions sont mis obligatoirement en majuscule de façon transparente dans la class Controls
/!\ Ainsi si vous utilisez l'héritage de Controls pour ajouter vos control/actions et utiliser je vous conseille très fortement
/!\ de vous en tenir à cette nomenclature.
/!\ 
/!\ car toutes les fonctions de Controls ayant rapport avec les Actions mettent en majuscule la variable actionName
/!\ en début de fonction

----- Utilité : -----
- ça évite les fautes de frappes car tout est en majuscule
- on repère plus facilement les actions dans le debugger

Utilisation :
let control = new ControlP1();
control.UP.isDown()

Création de votre propre class héritant de Controls :
Recopiez ce modèle Ajouter/Suprimer des Actions selons vos besoins
Regardez l'interface ActionContructor pour avoir la liste de ce que vous pouvez mettre à l'instanciation d'une Action
les Actions ne sont pas limités au keyboard...
*/
class playerControl extends United.StackControls.Controls {
    
    UP : United.StackControls.Action = new United.StackControls.Action({
        keyboard: ["Z", "W", "UP"]
    });

    LEFT : United.StackControls.Action = new United.StackControls.Action({
        keyboard: ["Q", "A", "LEFT"]
    });

    RIGHT : United.StackControls.Action =  new United.StackControls.Action({
        keyboard: ["D", "RIGHT"]
    });

    DOWN : United.StackControls.Action = new United.StackControls.Action({
        keyboard: ["S", "DOWN"]
    });

    INTERACT : United.StackControls.Action = new United.StackControls.Action({
       keyboard: ["E","SPACE"] 
    });

    CONSUME : United.StackControls.Action = new United.StackControls.Action({
       keyboard: ["P"]
    });

    ATTACK : United.StackControls.Action = new United.StackControls.Action({
       mButton: [0] 
    });

    ABSORB : United.StackControls.Action = new United.StackControls.Action({
       mButton: [2] 
    });

    __ROULADE : United.StackControls.Action = new United.StackControls.Action({
        keyboard:["SHIFT"]
    });

    roulade() : boolean {
        return this.__ROULADE.isDown() && (this.LEFT.wasJustPressed() || this.RIGHT.wasJustPressed())
    }

    constructor(){super(null)}
}
/*,
        gamepad: {
            0: {
                axis : {
                    0 : {
                        x : {
                            direction: '>',
                            value: 0.5
                        }
                    }
                }
            }
        }*/