const HorsGame_CSS : United.Stylesheets = new United.Stylesheets( (CSS: any) => {
    
    CSS.button_play = {
        text : "Histoire mode",
        click : function() {
            Sup.log("button play clicked!");
            HorsGame.$.enterGame();
        }
    } 
    
    return CSS;
});