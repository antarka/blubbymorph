var Game : United.game = new United.game("Jeu2D");
var HorsGame : United.game = new United.game("Horsgame");

var firstDialogue: boolean = false;

interface saveData {
    firststart: boolean;
    progression: number;
    healpotion: number;
    score: number;
    max_attack_bonus: number;
    regenLife_multiplier: number;
    shootDivided: number;
}

const dataName : string = "LD35_blubbystorage";
let data : saveData = United.storage.load<saveData>(dataName,{
    firststart : true,
    progression : 0,
    healpotion: 0,
    score: 0,
    max_attack_bonus: 0,
    regenLife_multiplier: 1,
    shootDivided: 0
});

const savedata = () => {
  United.storage.save(dataName,data);
}

const playerMeta = {
    transformation : 0,
    available : [0, 3],
    skins : [
        {
            sprite : "Assets/sprites/Characters/Slime",
            fire : "Assets/prefabs/projectiles/blubbyball",
            maxlife : 15,
            speedMultiplier: 1,
            attackMultiplier: [1, 5, 1, 1, 1] //Slime, Earth, Fire, Ice, Air
        },
        {
            sprite : "Assets/sprites/Monsters/Earth",
            fire : "Assets/prefabs/projectiles/earthball",
            maxlife : 35,
            speedMultiplier: 0.75,
            attackMultiplier: [3, 2, 4, 1, 2] //Slime, Earth, Fire, Ice, Air
        },
        {
            sprite : "Assets/sprites/Monsters/Fire",
            fire : "Assets/prefabs/projectiles/fireball",
            maxlife : 20,
            speedMultiplier: 1.3,
            attackMultiplier: [3, 1, 2, 4, 1] //Slime, Earth, Fire, Ice, Air
        },
        {
            sprite : "Assets/sprites/Monsters/Ice",
            fire : "Assets/prefabs/projectiles/iceball",
            maxlife : 30,
            speedMultiplier: 1,
            attackMultiplier: [3, 1, 3, 2, 5] //Slime, Earth, Fire, Ice, Air
        },
        {
            sprite : "Assets/sprites/Monsters/Air",
            fire : "Assets/prefabs/projectiles/fireball",
            maxlife : 20,
            speedMultiplier: 1.5,
            attackMultiplier: [3, 3, 5, 3, 2] //Slime, Earth, Fire, Ice, Air
        }
    ]
}

const P1 : United.Audio.Playlist = new United.Audio.Playlist({
    0 : {
        asset : "Assets/musiques/mus_superpowers",
        title : "Superpowers community musique",
        defaultVolume : 0.135
    }
}, {
    defaultTrack : 0,
    autoStart : false
});

/*const P2 : United.Audio.Playlist = new United.Audio.Playlist({
    0 : {
        asset : "Assets/musiques/M3",
        title : "DBZ Cover!",
        defaultVolume : 0
    }
}, {
    defaultTrack : 0,
    autoStart : false
}); */

United.Audio.Core.addPlaylist("Normal",P1);
//United.Audio.Core.addPlaylist("Donjon",P2);
United.Audio.Core.setPlaylist("Normal");
