/**
Game Design
------------------------------------------------------------------------------------------------------------------------------------
-->Concept:

On incarne un métamorphe qui a le pouvoir d'absorber un ennemi pour être immunisé à ce type d'ennemi et d'utiliser leurs compétences.
Plus il absorbe le même type de créatures plus son niveau de maîtrise de ce type de créatures augmentent et il peut altérer son
environnement, modifiant ainsi la map à son avantage ou à son désavantage.
Il peut aussi être une chimère et utiliser toutes les compétences des créatures ainsi absorbés,
et être immunisé à toutes les créatures pendant un laps de temps court.
Le but est de survivre à toutes les vagues.

------------------------------------------------------------------------------------------------------------------------------------
-->Compétences naturelles:

*Absorption ennemi: 
absorbe l'ennemi pour devenir l'ennemi et obtenir les mêmes compétences que lui

*Absorption projectile:
absorbe un projectile et le stock pour le recracher

------------------------------------------------------------------------------------------------------------------------------------
-->Créatures :

-Terre: 1 golem + 1 slime
* pousse les rochers
* crée un tremblement de terre en AoE
* change l'eau en boue, afin que si un ennemi passe dessus soit pris dans les marécages et ne bouge plus pour un temps
* change la lave en terre

-Feu:  1 golem + 1 slime
* passe à travers la lave et le feu
* tire des boules de feu
* l'herbe brûle sous ses pieds pour laisser place à du feu
* la terre devient de la lave sous ses pieds

-Eau:  1 golem + 1 slime
* marche sur l'eau
* se soigne grâce à l'eau en consommant la case eau sur laquelle il se trouve
* le sol se change en eau sous ses pieds

-Air:  1 golem + 1 slime
* peut atteindre des zones en hauteur
* peut pousser ou faire reculer les ennemis et/ou objets
* se déplace très vite à la limite de la téléportation

------------------------------------------------------------------------------------------------------------------------------------
-->Mode de jeu:

-Mode histoire:
On est un blob métamorphe, on apparait sur une carte contenant tous les boss, chacun dans son milieu naturel, 
des obstacles nous barrent la route.
On doit rejoindre le premier boss pour reccuperer sa compétence elementaire, un donjon (l'interieur du Boss) se lance alors.
Une fois termine nous avons la possibilite d'absorber les ennemis de son type afin de reccuperer leurs compétences
et ainsi avancer vers le boss suivant.

-Survie:
On incarne le blob ayant tous les pouvoirs du mode histoire et nous devons survivre a des vagues successives d'ennemies.
Il a me pouvoir chimérique débloquer dans ce mode.
Au fur et à mesure qu'il tue des ennemis, il obtient des points en adéquations avec le type d'ennemi.
Il place ces points dans un arbre de compétences afin d'augementer les effets de ses compétences.
*/
