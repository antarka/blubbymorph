namespace Utils {

    export function merge(obj1, obj2) {
        for (const p in obj2) {
            try {
                obj1[p] = ( obj2[p].constructor==Object ) ? merge(obj1[p], obj2[p]) : obj2[p];
            }
            catch(e) {
                obj1[p] = obj2[p];
            }
        }
        return obj1;
    }

    export function objectSize(obj : any) : number {
        let size : number = 0;
        for(const key in obj) {
            if (obj.hasOwnProperty(key))
                size++;
        }
        return size;
    };
    
    export function generateRandomVector2(range:number) : Sup.Math.Vector2 {
        const x : number = Sup.Math.Random.float(-range, range);
        const y : number = Sup.Math.Random.float(-range, range);
        return new Sup.Math.Vector2(x, y);
    }
    
    export function getRandom(min, max) {
      return Math.random() * (max - min) + min;
    }

    export function clampNumber(number : number,clamp : [number,number]) : number {
        return Math.min(Math.max(number, clamp[0]), clamp[1]);
    }

    const _hasOwnProperty = Object.prototype.hasOwnProperty;
    export const has = function(obj: any, prop: any) : boolean {
        return _hasOwnProperty.call(obj, prop);
    }

    export function randomObject(object:any) : any {
        const keys = Object.keys(object);
        return object[keys[ keys.length * Math.random() << 0]];
    }

    export function isFunction(func: any): boolean {
        return (typeof func) === 'function';
    }

    export function isUndefined(obj: any): boolean {
        return (typeof obj) === 'undefined';
    }

    export function isString(obj: any): boolean {
        return Object.prototype.toString.call(obj) === '[object String]';
    }

}

namespace United {

    export module Collections {

        export class Set<T> {

            private storage : T[];

            constructor(Table? : T[]) {
                this.storage = [];
                if(Table) Table.forEach((v : T) => {this.add(v)});
            }

            public add(value: T | T[]) : boolean {
                if(value instanceof Array) {
                    const myArray : T[] = <T[]>value;
                    myArray.forEach( (element: T ) => {
                        this.add(element);
                    });
                    return true;
                }
                this.storage.push(<T>value);
                return true;
            }

            public del(value : T | T[]) : boolean {
                if(value instanceof Array) {
                    const myArray : T[] = <T[]>value;
                    myArray.forEach( (element: T ) => {
                        if(this.has(element))
                            this.storage.splice(this.storage.indexOf(element),1);
                    });
                    return true;
                }
                if(this.has(<T>value)) {
                    this.storage.splice(this.storage.indexOf(<T>value),1);
                    return true;
                }
                return false;
            }

            public join = (separator?: string) : string => this.storage.join(separator);
            public shift = () : T => this.storage.shift();
            public pop = () : T => this.storage.pop();
            public peek = () : T => this.storage[0];
            public last = () : T => this.storage[this.size];

            public map(action: (value : T) => T) : void {
                let ret : T;
                this.forEach( (element : T, key : number ) => {
                    ret = action(element);
                    if(ret)
                        this.storage[key] = ret;
                });
            }

            public set(o: T,new_value: T) : boolean {
                if(this.has(o)) {
                    this.storage[this.storage.indexOf(o)] = new_value;
                    return true;
                }
                return false;
            }

            public has(value: T | T[]) : boolean {
                if(value instanceof Array) {
                    for(const k in value)
                        if(!this.has(value[k]))
                            return false;
                    return true;
                }
                return (this.storage.indexOf(<T>value) != -1) ? true : false;
            }

            public intersection(otherSet: Set<T>) : void {
                this.forEach( (element: T) => {
                    if(!otherSet.has(element)) {
                        this.del(element);
                    }
                });
            }

            public difference(otherSet: Set<T>) : void {
                otherSet.forEach( (element: T) => {
                    this.del(element);
                });
            }

            public union(otherSet: Set<T>) : void {
                otherSet.forEach( (element: T) => {
                    if(!otherSet.has(element))
                        this.add(element);
                });
            }

            public clone() : Set<T> {
                const temp : Set<T> = new Set<T>();
                this.forEach( (element: T) => {
                    temp.add(element);
                });
                return temp;
            }

            public isSubsetOf(otherSet: Set<T>) : boolean {
                if(this.size != otherSet.size)
                    return false;
                for(const k in this.storage)
                    if(!otherSet.has(this.storage[k]))
                        return false;
                return true;
            }

            public forEach(callback) : void {
                this.storage.forEach(callback);
            }

            public get size() : number {
                return this.storage.length;
            }

            public get isEmpty() : boolean {
                return this.storage.length <= 0;
            }

            public clear() : void {
                this.storage = [];
            }

            public toString = () : string => this.storage.toString();
            public toArray = () : T[] => this.storage;

        }

        export interface LinkPair<T> {
            key : string
            value : T
        }

        export class Link<T> {

            protected table : { [key:string] : T };
            protected numerique : string[];
            public generic : T;

            constructor() {
                this.table = {};
                this.numerique = [];
            }

            public add(key: string,value: T) : void {
                if(!Utils.has(this.table,key)) {
                    this.table[key] = value;
                    this.numerique.push(key);
                }
            }

            public update(key: string,new_value : any) : void {
                if(Utils.has(this.table,key)) {
                    this.table[key] = new_value;
                }
            }

            public clone() : Link<T> {
                let temp : Link<T> = new Link<T>();
                this.forEach((V:T,K:string) => {
                    temp.add(K,V);
                });
                return temp;
            }

            public merge(merged : Link<any>) : void {
                if(this === merged) {
                    merged.forEach((V:T,K:string) => {
                        this.add(K,V);
                    });
                }
            }

            public forEach(callback: (value:T,key?:string) => any) : void {
                let ret;
                for(const K in this.table) {
                    const V : T = this.table[K];
                    ret = callback(V,K);
                    if(ret == false) {
                        return;
                    }
                }
            }

            public remove(key : string) : void {
                if(Utils.has(this.table,key)) {
                    delete this.table[key];
                    const index : number = this.numerique.indexOf(key);
                    this.numerique.splice(index,1);
                }
            }

            public getValues(key: string) : T {
                if(Utils.has(this.table,key)) {
                    return this.table[key];
                }
                return null;
            }

            public getID(id : number) : string {
                if(this.numerique[id]) {
                    return this.numerique[id];
                }
            }

            public first() : LinkPair<T> {
                const key : string = this.numerique[0];
                const value : T = this.table[key];
                return { key : key , value : value};
            }

            public has(key: string) : boolean {
                return Utils.has(this.table,key);
            }

            public nextOf(key:string) : string {
                const index : number = this.numerique.indexOf(key);
                return (index == this.size) ? this.numerique[0] : this.numerique[index+1];
            }

            public prevOf(key:string) : string {
                const index : number = this.numerique.indexOf(key);
                return (index == 0) ? this.numerique[this.size] : this.numerique[index-1];
            }

            public get size() : number {
                return this.numerique.length;
            }

            public get isEmpty() : boolean {
                return this.numerique.length <= 0;
            }

            public clear() : void {
                this.table = {};
                this.numerique = [];
            }

        }
        
        export interface DictionnaryPair<K,V> {
            key : K;
            value : V;
        }

        export class Dictionnary<K,V> {

            protected table : { [key:string] : DictionnaryPair<K,V> } = {};
            protected count : number;

            constructor() {
                this.count = 0;
            }

            public add(key: K,value: V) : boolean {
                if(!this.has(key)) {
                    this.table[key.toString()] = { key : key , value : value };
                    return true;
                }
                return false;
            }

            public remove(key : string | K) : boolean {
                if(this.has(key)) {
                    delete this.table[`${key}`];
                    return true;
                }
                return false;
            }

            public getValue(key : string | K) : V {
                if(this.has(key))
                    return this.table[`${key}`].value;
                return null;
            }

            public setValue(key : string | K,value: V) : boolean {
                if(this.has(key)) {
                    this.table[`${key}`].value = value;
                    return true;
                }
                return false;
            }

            public keys() : K[] {
                const keys : K[] = [];
                this.forEach((key: K,value: V) => {
                    keys.push(key);
                });
                return keys;
            }

            public values() : V[] {
                const values : V[] = [];
                this.forEach((key: K,value: V) => {
                    values.push(value);
                });
                return values;
            }

            public forEach(callback: (key: K,value: V) => any) : void {
                for(const name in this.table) {
                    const pair: DictionnaryPair<K, V> = this.table[name];
                    const ret = callback(pair.key, pair.value);
                    if (ret === false) {
                        return;
                    }
                }
            }

            public clone() : Dictionnary<K,V> {
                const temp : Dictionnary<K,V> = new Dictionnary<K,V>();
                this.forEach((key: K,value: V) => {

                });
                return temp;
            }

            public union(otherDictionnary : Dictionnary<K,V>) : void {
                otherDictionnary.forEach( (k: K,v: V) => {
                    if(!this.has(k))
                        this.add(k,v);
                });
            }

            public has(key : string | K) : boolean {
                if(this.table[`${key}`])
                    return true;
                return false;
            }

            public clear() : void {
                this.table = {};
            }

            public get size() : number {
                return this.count;
            }

            public get isEmpty() : boolean {
                return this.count <= 0;
            }

            public toString(): string {
                let toret : string = "{";
                this.forEach((k : K, v : V) => {
                    toret = toret + "\n\t" + k.toString() + " : " + v.toString();
                });
                return toret + "\n}";
            }

        }

    }

}
