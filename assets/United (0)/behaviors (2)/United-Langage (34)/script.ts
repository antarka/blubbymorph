class Langage extends Sup.Behavior {
    
    public static Actors : Sup.Actor[];
    
    key : string; 
    private stored_langage : string;

    awake() {
        if(!this.key) {
            const potential : string = this.actor.getName().substr(United.langage.keyword.length);
            if(United.langage.has(potential)) {
                this.key = potential;
            }
            else {
                Sup.log("Impossible de trouver une clé lang pour l'acteur => "+this.actor.getName());
                this.destroy();
                delete this;
            }
        }
        this.stored_langage = United.langage.active;
        this.text(United.langage.get(this.key));
        Langage.Actors.push(this.actor);
    }

    text(text: string) : void {
        if(this.actor.textRenderer) {
            this.actor.textRenderer.setText(text);
        }
        else {
            Sup.log("Impossible de trouver un textRenderer pour Lang API sur l'acteur => "+this.actor.getName());
        }
    }

    update() {
        if(this.stored_langage != United.langage.active) {
            this.stored_langage = United.langage.active;
            this.text(United.langage.get(this.key));
        }
    }
}
Sup.registerBehavior(Langage);
