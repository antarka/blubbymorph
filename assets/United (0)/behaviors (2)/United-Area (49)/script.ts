namespace United {
    
    export class AreaBehavior extends Sup.Behavior {

        protected _area : United.Area;
        protected target : Sup.Actor;
        protected area_length : number = 2;
        private in_active : boolean = true;
        protected unique_in : boolean = false;

        protected configure($?:any) {}

        start() {
            this.configure(United.Game.$);
            this._area = new United.Area(this.actor,this.target,this.area_length);
        }

        protected in($?: any) {}
        protected out($?: any) {}

        protected set area(area: number) {
            this.area_length = area;
        }

        protected get area() {
            return this.area_length;
        }

        protected process() {}

        update() {
            this.process();
            if(this._area.in() && this.in_active) {
                if(this.unique_in) {
                    this.in_active = false;
                } 
                this.in(United.Game.$);
            }
            if(this._area.out()) {
                this.out(United.Game.$);
            }
        }
    
    }   
}