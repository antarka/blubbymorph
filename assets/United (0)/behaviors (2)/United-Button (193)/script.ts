namespace United {

    export interface ButtonReseatable {
        sprite : boolean;
        opacity : boolean;
        scale : boolean;
        text: boolean;
        position : boolean;
        textColor : boolean;
        textOpacity : boolean;
        eulerAngles : boolean;
    }

    export class Button {
        
        public haveText : boolean = false;
        public haveSprite : boolean = false;
        
        public myActor : Sup.Actor;
        public myBehavior : United.ButtonBehavior;
        
        constructor(actorName: string) {
            const Actor : Sup.Actor = Sup.getActor(actorName);
            if(Actor) {
                const behavior : United.ButtonBehavior = Actor.getBehavior(United.ButtonBehavior);
                if(!behavior) {
                    Actor.addBehavior(United.ButtonBehavior);
                }
                this.myBehavior = behavior;
                this.myActor = Actor;
            }
            else {
                Sup.log("Not possible to create button with a invalide Actor");
            }
        }

        public setReset(key: string,value: boolean) {
            this.myBehavior.reseatable[key] = value;
        }

        public sprite(sprite:string,replace?:boolean) : void {
            this.myBehavior.setSprite(sprite,replace);
        }
        
        // Action !! 
        public set click(action: (self: United.ButtonBehavior) => void) {
            this.myActor["click"] = action;
        }

        public set hover(action: (self: United.ButtonBehavior) => void) {
            this.myActor["hover"] = action;
        }

        public set unhover(action: (self: United.ButtonBehavior) => void) {
            this.myActor["unhover"] = action;
        }

        public set rightclick(action: (self: United.ButtonBehavior) => void) {
            this.myActor["rightclick"] = action;
        }

        public set middleclick(action: (self: United.ButtonBehavior) => void) {
            this.myActor["middleclick"] = action;
        }

        public set doubleclick(action: (self: United.ButtonBehavior) => void) {
            this.myActor["doubleclick"] = action;
        }
        
    }

    export class ButtonBehavior extends Sup.Behavior {

        public static actions : string[] = ["click","hover","unhover","doubleclick","rightclick","middleclick"];
        public static resetActions : { [key:string] : boolean } = {
            "unhover" : true
        }

        // Behavior property
        public isHover : boolean = true; 
        public animationFrame : number = 25;
        public animatedSprite : boolean = true;

        private doubleClick : boolean = false;
        private doubleClickInterval : any;
        private textRenderer : Sup.TextRenderer;
        private creativeSprite : Sup.Actor; 

        private spriteTick : number;
        private spriteAnimationState : boolean = true;

        public defaultSprite : string;
        public defaultOpacity : number;
        public defaultScale : Sup.Math.Vector3 | Sup.Math.XYZ;
        public defaultPosition : Sup.Math.Vector3 | Sup.Math.XYZ;
        public defaultEuler : Sup.Math.Vector3 | Sup.Math.XYZ;
        public defaultText : string;
        public defaultColor : Sup.Color;
        public defaultTextOpacity : number;

        public resetBehavior : boolean = true;
        public reseatable : ButtonReseatable = {
            sprite : true,
            opacity : true,
            scale : true,
            text : true,
            position : false,
            eulerAngles : false,
            textOpacity : true,
            textColor : true
        }

        awake() {
            ButtonBehavior.actions.forEach( (action : string) => {
                this.actor[action] = undefined;
            });
            this.onAwake();
        }

        start() {
            if(this.actor.spriteRenderer) {
                this.defaultSprite = this.actor.spriteRenderer.getSprite().path;
                this.defaultOpacity = this.actor.spriteRenderer.getOpacity();
                this.creativeSprite = new Sup.Actor("creativeSprite",this.actor);
                this.creativeSprite.setLocalPosition(new Sup.Math.Vector3(0,0,0.1));
                const Sprite: Sup.SpriteRenderer = new Sup.SpriteRenderer(this.creativeSprite,this.defaultSprite);
                Sprite.setOpacity(0);
            }
            this.defaultScale = this.actor.getLocalScale();
            this.defaultPosition = this.actor.getLocalPosition();
            this.defaultEuler = this.actor.getLocalEulerAngles();

            // Try to get a text ! 
            {
                const Actor : Sup.Actor = this.actor.getChildren[0];
                if(Actor) {
                    if(Actor.textRenderer) {
                        this.textRenderer = Actor.textRenderer;
                        this.defaultText = Actor.textRenderer.getText();
                        this.defaultOpacity = Actor.textRenderer.getOpacity();
                        this.defaultColor = Actor.textRenderer.getColor();
                    }
                }
            }
            this.onStart();
        }

        public setText(text: string,replace?: boolean) : void {
            if(!this.textRenderer) {
                const textActor: Sup.Actor = new Sup.Actor("texte",this.actor);
                textActor.setLocalPosition(new Sup.Math.Vector3(0,0,0.2));
                const textRenderer : Sup.TextRenderer = new Sup.TextRenderer(textActor,text,United.Font.Default);
                this.defaultText = text;
                this.textRenderer = textRenderer;
                this.defaultOpacity = textRenderer.getOpacity();
                this.defaultColor = textRenderer.getColor();
            }
            else {
                this.textRenderer.setText(text);
                if(replace) {
                    this.defaultText = text;
                }
            }
        }

        public setColor(color: Sup.Color,replace?: boolean) : void {
            if(this.textRenderer) {
                this.textRenderer.setColor(color);
                if(replace) {
                    this.defaultColor = color;
                }
            }
        }

        public setTextOpacity(opacity: number,replace?: boolean) : void {
            if(this.textRenderer) {
                opacity = Math.min(Math.max(opacity, 0), 1);
                this.actor.textRenderer.setOpacity(opacity);
                if(replace) {
                    this.defaultTextOpacity = opacity;
                }
            }
        }

        public setSprite(sprite: string,replace?: boolean) : void {
            if(this.defaultSprite) {
                if(this.animatedSprite) {
                    if(!this.spriteTick) {
                        this.spriteTick = 0;
                        this.spriteAnimationState = true;
                        this.creativeSprite.spriteRenderer.setSprite(sprite);
                        this.creativeSprite.spriteRenderer.setOpacity(0);
                    }
                }
                else {
                    this.actor.spriteRenderer.setSprite(sprite);
                }
                if(replace) {
                    this.defaultSprite = sprite;
                }
            }
        }

        public setSpriteAnimation(animation: string) : void {
            if(this.defaultSprite) {
                this.actor.spriteRenderer.setAnimation(animation);
            }
        }

        public get sprite() : Sup.SpriteRenderer {
            if(this.defaultSprite)
                return this.actor.spriteRenderer;
        }

        public setOpacity(opacity: number,replace?: boolean) : void {
            if(this.defaultSprite) {
                opacity = Math.min(Math.max(opacity, 0), 1);
                this.actor.spriteRenderer.setOpacity(opacity);
                if(replace) {
                    this.defaultOpacity = opacity;
                }
            }
        }

        public setScale(scale: Sup.Math.Vector3 | Sup.Math.XYZ,replace?: boolean) : void {
            this.actor.setLocalScale(scale);
            if(replace) {
                this.defaultScale = scale;
            }
        }

        public setPosition(position: Sup.Math.Vector3 | Sup.Math.XYZ,replace?: boolean) : void {
            this.actor.setLocalPosition(position);
            if(replace) {
                this.defaultPosition = position;
            }
        }

        public setEuler(euler: Sup.Math.Vector3 | Sup.Math.XYZ,replace?: boolean) : void {
            this.actor.setLocalEulerAngles(euler);
            if(replace) {
                this.defaultEuler = euler;
            }
        }

        private reset() : void {

            if(this.defaultSprite) {
                if(this.reseatable.sprite) {
                    if(this.actor.spriteRenderer.getSprite().path != this.defaultSprite) {
                        this.actor.spriteRenderer.setSprite(this.defaultSprite);
                    }
                }

                if(this.reseatable.opacity) {
                    if(this.actor.spriteRenderer.getOpacity() != this.defaultOpacity) {
                        this.actor.spriteRenderer.setOpacity(this.defaultOpacity);
                    }
                }
            }

            if(this.reseatable.scale && this.actor.getLocalScale() != this.defaultScale) {
                this.actor.setLocalScale(this.defaultScale);
            }

            if(this.reseatable.position && this.actor.getLocalPosition() != this.defaultPosition) {
                this.actor.setLocalPosition(this.defaultPosition);
            }

            if(this.reseatable.eulerAngles && this.actor.getLocalEulerAngles() != this.defaultEuler) {
                this.actor.setLocalEulerAngles(this.defaultEuler);
            }

            if(this.textRenderer) {
                if(this.reseatable.text && this.textRenderer.getText() != this.defaultText) {
                    this.textRenderer.setText(this.defaultText);
                }

                if(this.reseatable.textOpacity && this.textRenderer.getOpacity() != this.defaultOpacity) {
                    this.textRenderer.setOpacity(this.defaultOpacity);
                }

                if(this.reseatable.textColor && this.textRenderer.getColor() != this.defaultColor) {
                    this.textRenderer.setColor(this.defaultColor);
                }
            }


        }

        private call(action: string) : void {
            if(this.actor[action] && this.actor[action] instanceof Function) {
                this.actor[action](this);
                if(this[action])
                    this[action]();
            }
            if(ButtonBehavior.resetActions[action] && this.resetBehavior) {
                this.reset();
            }
        }

        // Sub behavior property
        protected hover() {};
        protected unhover() {}; 
        protected click() {};
        protected rightclick() {};
        protected middleclick() {};
        protected doubleclick() {};
        protected onAwake() {};
        protected onStart() {};
        protected onUpdate() {};

        public update() : void {
            
            // Sprite creative Animation !
            if(this.spriteTick != undefined) {
                const factor : number = this.spriteTick / this.animationFrame;
                const progression : number = (1 - (1 - factor) * (1 - factor));
                const opacity : number = progression;
                this.creativeSprite.spriteRenderer.setOpacity(opacity);
                
                if(this.spriteAnimationState) {
                    if(this.spriteTick >= this.animationFrame) {
                        this.spriteTick = this.animationFrame;
                    }
                    else {
                        this.spriteTick++;
                    }
                }
                else {
                    if(this.spriteTick == 0) {
                        this.spriteTick = undefined;
                        this.creativeSprite.spriteRenderer.setOpacity(0);
                    }
                    else {
                        this.spriteTick--;
                    }
                }
            }

            // Rayon
            if(United.UIRay.intersectActor(this.actor,false).length > 0) {
                if(!this.isHover) {
                    this.isHover = true;
                    this.call("hover");
                    if(this.spriteTick && !this.spriteAnimationState) {
                        this.spriteAnimationState = true;
                    }
                }

                // Left
                if(Sup.Input.wasMouseButtonJustPressed(0)) {
                    this.call("click");
                    if(this.doubleClick) {
                        this.call("doubleclick");
                        this.doubleClick = false;
                        Sup.clearInterval(this.doubleClickInterval);
                    }
                    else {
                        this.doubleClick = true;
                        this.doubleClickInterval = Sup.setTimeout(280, () => {
                            this.doubleClick = false;
                        });
                    }
                }

                // Middle
                if(Sup.Input.wasMouseButtonJustPressed(1)) {
                    this.call("middleclick");
                }

                // Right
                if(Sup.Input.wasMouseButtonJustPressed(2)) {
                    this.call("rightclick");
                }
            }
            else {
                if(this.isHover) {
                    this.isHover = false;
                    this.call("unhover");
                    if(this.spriteTick) {
                        this.spriteAnimationState = false;
                    }
                }
            }
            
            this.onUpdate();

        }
    }
    Sup.registerBehavior(United.ButtonBehavior);

}
