namespace United {
  
    export interface dt_constructor {
        dt : number
        auto? : boolean
        loop? : boolean
    }

    export class dt {

        public static default_start : boolean = true;
        public static default_while : boolean = true;

        private _started : boolean = false;
        private _dt : number;
        private _tick : number;
        private while : boolean;

        constructor(conf : dt_constructor) {
            this._dt = conf.dt;
            this.start = conf.auto || United.dt.default_start;
            this.while = conf.loop || United.dt.default_while;
            this._tick = 0;
        }

        public get start() : boolean {
            return this._started;
        }

        public set start(state: boolean) {
            this._started = state;
        }

        public get dt() : number {
            return this._dt;
        }
    
        public get tick() : number {
            return this._tick;
        }
    
        public reset() : void {
            this._tick = 0;
        }

        public set(dt : number,reset?: boolean) : void {
            this.dt = dt;
            if(reset) 
                this._tick = 0;
        }

        public walk() : boolean {
            if(this.start) {
                if(this._tick >= this.dt) {
                    this._tick = 0;
                    if(!this.while) {
                        this.start = false;
                    }
                    return true;
                }
                this._tick++;
            }
            return false;
        }

    }

    export class timer {
        
        public static default_start : boolean = true;

        private _tick : number = 0;
        private max : number; 
        private action : United.callback<void>;
        private _started : boolean;

        constructor(start?: boolean) {
            this._started = start || timer.default_start;
        }

        public get(format:string) : string {
            const seconde : number  = this.seconde;
            const seconde_0 : string = (seconde < 10) ? "0" : "";
            const minute : number   = this.minute;
            const minute_0 : string = (minute < 10) ? "0" : "";
            const hours : number    = Math.floor(minute / 60);

            format = format.replace("$s",seconde.toString());
            format = format.replace("$S",seconde_0+seconde.toString());
            format = format.replace("$m",minute.toString());
            format = format.replace("$M",minute_0+minute.toString());
            format = format.replace("$h",hours.toString());

            return format;
        }

        public get start() : boolean {
            return this._started;
        }

        public set start(state: boolean) {
            this._started = state;
        }

        public get tick() : number {
            return this._tick;
        }

        public setMax(tick: number,action: United.callback<void>) : void {
            this.max = tick;
            this.action = action;
        }

        public get seconde() : number {
            return Math.floor(this.tick / 60) % 60;
        }

        public get minute() : number {
            const seconde : number = Math.floor(this.tick / 60);
            return Math.floor(seconde / 60) % 60;
        }

        public reset() : void {
            this.tick = 0;
        }

        public update() : void {
            if(this.start) {
                this.tick++;
                if(this.max != undefined) {
                    if(this.tick > this.max) {
                        this.action();
                        this.start = false;
                    }
                }
            }
        }

    }
  
}