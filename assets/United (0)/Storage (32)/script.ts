namespace United {
  
    export class storage {

        public static load<T>(dataName: string,backdoor:T) : T {
            try {
                let O = localStorage.getItem(dataName);
                if(O) {
                    return JSON.parse(O);
                }
                else {
                    if(backdoor) {
                        this.save(dataName,backdoor);
                    }
                }
                return backdoor;
            }
            catch(Ex) {
                return backdoor;
            }
        }

        public static save(dataName: string,data: any) : boolean {
            try {
                localStorage.setItem(dataName, JSON.stringify(data));
                return true;
            }
            catch(Ex) {
                return false;
            }
        } 

        public static clear() : boolean {
            try {
                localStorage.clear();
                return true;
            }
            catch(Ex) {
                return false;
            }
        }

    }
  
}