declare var window : Window;
declare var localStorage : Storage;

namespace United {
  
    export let preventUpdateCollisions : boolean = true;
    
    export const fps : number = Sup.Game.getFPS();
    export let Game : United.game;
    
    export type callback<T> = () => T;
  
    export class game {
        
        public name : string;
        public pause : boolean = false;
    
        protected _state : boolean = true;
        protected _triggering : boolean = true;
        protected plugins : any[] = [];
        public Chunk : United.Chunk = new United.Chunk();
    
        protected ownTriggers : United.callback<any>[] = [];
        protected ownReset : United.callback<any>[] = [];
        protected ownChunk : United.Chunk[] = [];
        protected ownScenes : Sup.Actor[][] = [];

        constructor(name: string) {
            this.name = name;
        }
    
        public init() : void {
            United.Game = this;
        }
    
        public get $() {
            return this.Chunk.$;
        }
    
        public reset() : void {
            this.Chunk.resetAll(); 
            this.ownReset.forEach( (behaviorFunction : () => any ) => {
                behaviorFunction();
            });
            
            this.ownChunk.forEach( (Chunk : United.Chunk ) => {
                Chunk.resetAll();
            });
        }
    
        public linkBehavior(behavior: () => any) : void {
            this.ownReset.push(behavior);
        }
    
        public linkChunk(Chunk: United.Chunk) : void {
            this.ownChunk.push(Chunk);
            Chunk.resetAll();
        }
    
        public loadObject(o: { [key:string] : any }) {
            this.Chunk.loadObject(o);
        }
    
        public declare(name: string,variable) : void {
            this.Chunk.declare(name,variable);
        }
    
        public remove(name: string) : boolean {
            return this.Chunk.remove(name);
        }
    
        public destroy() { // Destructor
            United.Worker.clear();
            this.plugins = [];
            this.ownChunk.forEach( (Chunk : United.Chunk ) => {
                Chunk.resetAll();
            });
        }

        public appendScene(path: string) {
            if(United.Tree.exist(path)) {
                const callback : United.callback<void> = () => {
                    const ActorList: Sup.Actor[] = Sup.appendScene(path);
                    this.ownScenes.push(ActorList);
                }
                this.ownTriggers.push(callback);
            }
        }

        public loadScene(path: string, cb?:United.callback<void>) {
            if(United.Tree.exist(path)) {
                this._triggering = false;
                const callback : United.callback<void> = () => {
                    Sup.loadScene(path);
                    if(cb) cb();
                    this.destroy();
                }
                this.ownTriggers.push(callback);
            }
        }

        public loadPlugin<T,I>(plugin: { new(configuration): T }, configuration: I,chunkName?: string) : T {
            const instance : T = new plugin(configuration);
            this.plugins.push(instance);
            if(chunkName) {
                this.Chunk.declare(chunkName,instance);
            }
            return instance;
        }

        public update() : void {
            United.Worker.update();
            United.Audio.Core.update();
            United.preventUpdateCollisions = true;
            
            // Boucle de gestion des déclencheurs propre à l'instance de jeu!
            if(this.ownTriggers.length > 0) {
                let ret;
                this.ownTriggers.forEach((callback : United.callback<any>,index : number) => {
                    ret = callback();
                    if(ret != false) {
                        this.ownTriggers.splice(index,1);
                    }
                });
            }
            
            // On update les plugins du jeu!
            this.plugins.forEach((v : any) => {
                v.update();
            });
        }

    }
  
}
import U = United;