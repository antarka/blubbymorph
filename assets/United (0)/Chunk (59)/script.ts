namespace United {
    
    export class Chunk {
        
        public $;
        protected duplica: { [key:string] : any };
        private GameRestrict : boolean = false;
        private GameName: string;
    
        constructor(defaultVars? : { [key:string] : any }) {
            this.$ = {};
            this.duplica = {};
            if(defaultVars) {
                this.loadObject(defaultVars);
            }
        }
    
        linkGame(game: United.game) {
            this.GameRestrict = true;
            this.GameName = game.name;
        }
    
        loadObject(o: { [key:string] : any }) {
            for(const key in o) {
                const variable = o[key];
                this.$[key] = variable;
                this.duplica[key] = variable;
            }
        }
        
        declare(name: string,variable) : void {
            this.$[name] = variable;
            this.duplica[name] = variable;
        }
    
        remove(name: string) : boolean {
            if(Utils.has(this.$,name)) {
                delete this.$[name]; 
                delete this.duplica[name];
                return true;
            }
            return false;
        }
    
        rename(name: string,newName: string) : boolean {
            if(Utils.has(this.$,name) && name != newName) {
                this.duplica[newName] = this.duplica[name];
                this.$[newName] = this.$[name];
                delete this.$[name];
                delete this.duplica[name];
                return true;
            }
            return false;
        }
        
        resetAll() : void {
            for(const key in this.$) this.$[key] = this.duplica[key];
        }
    
        resetVar(name: string) : boolean {
            if(Utils.has(this.$,name)) {
                this.$[name] = this.duplica[name];
                return true;
            }
            return false;
        }
    
        clear() : void {
            this.$ = {};
            this.duplica = {};
        }
        
    }

}