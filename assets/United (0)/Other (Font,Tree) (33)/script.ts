namespace United {
    
    export const Path : string = "United/fonts/";

    export class Font {

        public static Default : string = `${Path}Exo-regular`;
        public static Exo : { [key:string] : string } = {
            regular :       `${Path}Exo-regular`
        }

        public static add(name:string, font:Sup.Asset) : boolean {
            if(!this[name]) {
                this[name] = font;
                return true;
            }
            return false;
        }

    }

    export module Tree {
        
        export function exist(path: string) : Sup.Asset {
            try {
                const Asset : Sup.Asset = Sup.get(path);
                return Asset;
            }
            catch(Err) {
                return null;
            }
        }
        
        export function is(path: string,type: any) {
            if(United.Tree.exist(path)) {
                let Asset : Sup.Asset = Sup.get(path);
                if(Asset instanceof type) {
                    return true;
                }
            }
            return false;
        } 
    }
    
}