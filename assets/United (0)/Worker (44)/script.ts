namespace United {
    
    export interface Worker_info {
        tick : number;
        duration : number;
    }

    export interface Worker_constructor {
        name : string;
        duration : number;
        callback : (info : United.Worker_info) => any;
        exec? : number;
        autoStart? : boolean;
        autoRestart? : boolean;
    }
    
    export class Worker {
        
        private static table = {};
        private static elementsStore = {};
        
        public name : string;
        public _start : boolean;
        public tick : number;
        public duration : number;
        public autoRestart : boolean;
        private exec : number;
        private callback : (info: United.Worker_info) => any;
        
        constructor(conf : United.Worker_constructor) {
            this.name = conf.name;
            this._start = conf.autoStart || false;
            this.tick = 0;
            this.duration = conf.duration;
            this.callback = conf.callback;
            this.exec = conf.exec || 0;
            this.autoRestart = conf.autoRestart || false;
        }
        
        public reset() : void {
            this.tick = 0;
        }
        
        public execute() : void {
            
        }
        
        public set start(value: boolean) {
            
        }
        
        public get start() : boolean {
            return this._start;
        }
        
        public static clear() {
            this.table = {};
        }
        
        public static update() {
            
        }
        
    }

}
