namespace United {
    
    export class Behavior extends Sup.Behavior {
        
        private _dt : United.dt = new United.dt({dt:United.fps});
        private _callbacklist : United.callback<any>[] = [];

        protected preventUpdateCollisions : boolean;
        protected active : boolean = true;
    
        protected each() {}
        protected onAwake($: any) {}
        protected onStart($: any) {}
        protected main($: any) {}
    
        protected next(callback: United.callback<any>) : void {
            this._callbacklist.push(callback);
        }
    
        protected call(triggerName: string) : void {
            United.triggers.call(triggerName);
        }
    
        awake() {
            this.onAwake(United.Game.$);
        }
    
        start() {
            this.onStart(United.Game.$);
        }
    
        update() {
            if(this.active) {
                this.preventUpdateCollisions = true;
                
                if(this._dt.tick == 1) {
                    if(this._callbacklist.length > 0) {
                        let ret;
                        this._callbacklist.forEach((callback : United.callback<any>,index : number) => {
                            ret = callback();
                            if(ret != false) {
                                this._callbacklist.splice(index,1);
                            }
                        });
                    }
                    this.each();
                }
                this._dt.walk();
            }
            this.main(United.Game.$);
        }

    }

}