namespace United {
    
    export interface trigger_constructor {
        name : string
        action : United.callback<any> 
        exec? : number
    }
        
    export type trigger_store = {
        [key: string] : United.triggers
    }
    
    export class triggers {
        
        protected static store : trigger_store = {};
    
        public name : string;
        public action : United.callback<any>;
        private exec : number;
        private countExecution : number;
        public active : boolean;
        
        constructor(conf : trigger_constructor) {
            this.countExecution = 0;
            this.active     = true;
            this.name       = conf.name;
            this.action     = conf.action;
            this.exec       = conf.exec || 0;
            triggers.store[this.name] = this;
        }
        
        public execute() : boolean {
            if(this.active) {
                if(this.exec != 0) {
                    this.action();
                    this.countExecution++;
                    if(this.countExecution == this.exec) {
                        delete United.triggers[this.name];
                        this.active = false;
                    }
                }
                else {
                    this.action();
                }
                return true;
            }
            return false;
        }
        
        public static call(name: string) : boolean {
            if(Utils.has(United.triggers.store,name)) {
                return United.triggers[name].execute();
            }
            return false;
        }
        
    }
    
}