namespace United {
    
    export module char {
        
        export const number : United.Collections.Set<string> = new United.Collections.Set<string>([
            "1","2","3","4","5","6","7","8","9","0"
        ]);

        export class filter {

            constructor(private _charTable: United.Collections.Set<string>) {}

            public get charTable() : United.Collections.Set<string> {
                return this._charTable;
            }

            public get textEntered() : string {
                const text : string = Sup.Input.getTextEntered();
                if(this._charTable.has(text)) 
                    return text;
                return null;
            }

        }
        
    }

}