namespace United {
    
    export class Screen {
        
    }

    export class Mouse {
        
        // Static short-cut
        public static get left() : boolean {
            return Sup.Input.wasMouseButtonJustPressed(0);
        }
    
        public static get middle() : boolean {
            return Sup.Input.wasMouseButtonJustPressed(1);
        }
    
        public static get right() : boolean {
            return Sup.Input.wasMouseButtonJustPressed(2);
        }
    
    }
    
}