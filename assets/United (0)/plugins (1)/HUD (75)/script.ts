namespace United {
    
    export let UIRay : Sup.Math.Ray;
    
    export interface HUDConstructor {
        camera: Sup.Camera,
        defaultContainer?: string
    }
    
    export class HUD {
        
        public static inner : Sup.Math.Vector3 = new Sup.Math.Vector3(0,0,-4);
        public static outer : Sup.Math.Vector3 = new Sup.Math.Vector3(200,0,-4);
        
        public Rayon : Sup.Math.Ray;

        private Camera : Sup.Camera;
        private UIScanner : United.Scanner;
        private Containers : { [key:string] : Sup.Actor } = {};
        private Container_Active : string;
        
        constructor(conf: United.HUDConstructor) {
            this.Camera = conf.camera;
            this.Rayon = new Sup.Math.Ray(this.Camera.actor.getPosition(),new Sup.Math.Vector3(0,0,-1)); 
            this.Rayon.setFromCamera(this.Camera,Sup.Input.getMousePosition());
            United.UIRay = this.Rayon;
            
            this.UIScanner = new United.Scanner(this.Camera.actor,["containers_","button_"]);
            let first: string;
            this.UIScanner.forEach("containers_",(actor: Sup.Actor) => {
                if(!first) {
                    first = actor.getName();
                }
                this.Containers[actor.getName()] = actor;
                actor.setPosition( United.HUD.outer.toVector2() );
            });
            
            this.UIScanner.forEach("button_",(actor: Sup.Actor) => {
                new United.Button(actor.getName());
            });
            
            if(conf.defaultContainer && this.Containers[conf.defaultContainer]) {
                this.Containers[conf.defaultContainer].setLocalPosition( United.HUD.inner.toVector2() );
                this.Container_Active = conf.defaultContainer;
            }
            else {
                this.Containers[first].setLocalPosition( United.HUD.inner.toVector2() );
                this.Container_Active = first;
            }
        }

        public setContainer(name: string) : boolean {
            if(this.Containers[name]) {
                this.Containers[this.Container_Active].setLocalPosition( United.HUD.outer.toVector2() );
                this.Containers[name].setLocalPosition( United.HUD.inner.toVector2() );
                this.Container_Active = name;
                return true;
            }
            return false;
        }
    
        public loadCSS(CSS: United.Stylesheets) : void {
            CSS.init();
        }
    
        public update() : void {
            United.UIRay.setFromCamera(this.Camera,Sup.Input.getMousePosition());
        }
        
    }
    
    export class Stylesheets {
        
        private CSS : any = {};
        private _init : boolean = false;
        
        constructor(actionStore: (CSS: any) => any) {
            this.CSS = actionStore(this.CSS);
        }

        getActors_formString(ExpActorName: string) : Sup.Actor[] {
            const ActorsList: string[] = ExpActorName.split(" ");
            const ActorsFinal : Sup.Actor[] = [];
            ActorsList.forEach( (actorName: string) => {
                let ret : Sup.Actor = Sup.getActor(actorName);
                if(ret) {
                    ActorsFinal.push(ret);
                }
            });
            return ActorsFinal;
        }

        private merge(o: any,source: any) : any {
            for(const i in o) {
                let env : any = o[i];
                if(typeof env == "object") 
                    for(let j in env) source[j] = env[j];
            }
            return source;
        }

        public static actionStore = {
            click : function(actor: Sup.Actor,action: any,depth: number) {
                if(depth > 1) return;
                const Button : United.Button = new United.Button(actor.getName());
                if(typeof action == "function") {
                    Button.click = action;
                }
                else {
                    Button.click = () => {
                        for(const Rules in action) {
                            if(United.Stylesheets.actionStore[Rules]) 
                                United.Stylesheets.actionStore[Rules](actor,Rules[action],2);
                        }
                    }
                }
            },
            hover : function(actor: Sup.Actor,action: any,depth: number) {
                if(depth > 1) return;
                const Button : United.Button = new United.Button(actor.getName());
                if(typeof action == "function") {
                    Button.hover = action;
                }
                else {
                    Button.click = () => {
                        for(const Rules in action) {
                            if(United.Stylesheets.actionStore[Rules]) 
                                United.Stylesheets.actionStore[Rules](actor,Rules[action],2);
                        }
                    }
                }
            },
            unhover : function(actor: Sup.Actor,action: any,depth: number) {
                if(depth > 1) return;
                const Button : United.Button = new United.Button(actor.getName());
                if(typeof action == "function") {
                    Button.unhover = action;
                }
                else {
                    Button.click = () => {
                        for(const Rules in action) {
                            if(United.Stylesheets.actionStore[Rules]) 
                                United.Stylesheets.actionStore[Rules](actor,Rules[action],2);
                        }
                    }
                }
            },
            sprite : function(actor: Sup.Actor,action: any,depth: number)  {
                const Button : United.Button = new United.Button(actor.getName());
                Button.myBehavior.setSprite(action,(depth <=1) ? true : false);
            },
            opacity : function(actor: Sup.Actor,action: any,depth: number)  {
                const Button : United.Button = new United.Button(actor.getName());
                Button.myBehavior.setOpacity(action,(depth <=1) ? true : false);
            },
            text : function(actor: Sup.Actor,action: any,depth: number)  {
                const Button : United.Button = new United.Button(actor.getName());
                Button.myBehavior.setText(action,(depth <=1) ? true : false);
            }
        }

        init() {
            Sup.log("init HUD CSS!");
            if(!this._init) {
                this._init = true;
                
                //
                // First occurence for extending!
                //
                for(const ExpActorName in this.CSS) {
                    let Rules = this.CSS[ExpActorName];
                    if(Rules.extends && Rules.extends.length > 0) {
                        Rules.extends.foreach( (O: any) => {
                            Rules = this.merge(O,Rules);
                        });
                        delete Rules.extends;
                    }
                }
                
                //
                // Apply action to the actor!
                //
                for(const ExpActorName in this.CSS) {
                    const ActorsList: Sup.Actor[] = this.getActors_formString(ExpActorName);
                    const Rules = this.CSS[ExpActorName];
                    
                    // On applique les actions à nos acteurs!
                    ActorsList.forEach( (actor: Sup.Actor) => {
                        for(const Action in Rules) {
                            if(United.Stylesheets.actionStore[Action]) {
                                United.Stylesheets.actionStore[Action](actor,Rules[Action],1);
                            }
                            else {
                                Sup.log(`The following CSS Action doesn't exist ${Action} on Actor ${actor.getName()}`);
                            }
                        }
                    });
                    
                }
            }
        }
        
    }

}