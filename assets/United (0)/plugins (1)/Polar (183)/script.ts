class Polar {
    public rayon : number;
    public theta : number;
    
    constructor(rayon, theta) {
        this.rayon = rayon;
        this.theta = theta;
    }

    toCarthesian() : Vector2 {
        return new Vector2 (this.rayon * Math.cos(this.theta), this.rayon * Math.sin(this.theta));
    }
}

class Vector2 extends Sup.Math.Vector2 {
    // Donne le putain de vecteur direction de sa mère du
    // Point/Vecteur from au Point/Vecteur to
    // (oui il y a fromto.subtract(tofrom) mais je sais jamais dans quel sens ça ce met)
    static getVector(from : Sup.Math.Vector2, to : Sup.Math.Vector2) : Vector2 {
        return new Vector2(to.x - from.x, to.y - from.y);
    }

    toPolar() : Polar {
        return new Polar(this.length(), this.angle());
    }

    set _angle(angle) {
        let v : any = this.toPolar();
        v.theta = angle;
        v = v.toCarthesian();
        
        this.x = v.x;
        this.y = v.y;
    }

    get _angle() {
        return this.angle();
    }

    set _length(length) {
        let v : any = this.toPolar();
        v.rayon = length;
        v = v.toCarthesian();
        
        this.x = v.x;
        this.y = v.y;
    }

    get _length() {
        return this.length();
    }
}