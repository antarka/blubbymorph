namespace United {
    
    export interface FadeConstructor {
        fadeActor: Sup.Actor;
        defaultState?: boolean;
        autoStart?: boolean;
        callback?: United.callback<any>;
    }
    
    export class Fade {
        
        private _state: boolean;
        private _dt : United.dt;
        public _start : boolean;
        private _actor: Sup.Actor;
        private _frame : number = 60;
        public _callback : United.callback<any>;
        
        constructor(conf : United.FadeConstructor) {
            this._actor = conf.fadeActor;
            this._dt = new United.dt({dt:this._frame});
            this._state = conf.defaultState || true;
            if(conf.callback) {
                this._callback = conf.callback;
            }
            this._start = (conf.autoStart) ? conf.autoStart : false;
            if(!this._actor.spriteRenderer) {
                console.log("Please define good actor spriteRenderer for fade!");
                return null;
            }
        }

        public start(callback?: United.callback<any>) {
            this._dt.reset();
            this._start = true;
            if(callback) {
                this._callback = callback;
            }
        }
                    
        public get state() : boolean {
            return this._state;
        }
                    
        public set state(value: boolean) {
            this._start = true;
            this._state = value;
            this._dt.reset();
        }
    
        public set opacity(value: number)  {
            this._actor.spriteRenderer.setOpacity(value);
        }
    
        public get opacity() : number {
            return this._actor.spriteRenderer.getOpacity();
        }
    
        update() : void {
            if(this._start) {
                if(this._dt.walk()) {
                    this._start = false;
                    this._state = !this._state;
                    this._dt.reset();
                    this.opacity = (this.state) ? 1 : 0;
                    if(this._callback) {
                        this._callback();
                    }
                    return;
                }
                const factor : number = this._dt.tick / this._frame;
                const progression : number = (1 - (1 - factor) * (1 - factor));
                this.opacity = (this.state) ? 1 - progression : progression;
            }
        }
        
    }
    
}