namespace United {
    
    export namespace Audio {

        export class Controller {
            
            private static controllerStore : { [key:string] : United.Audio.Controller } = {};
            
            private store: Sup.Audio.SoundPlayer[];
            public name: string;
            public defaultVolume: number;
            public volume: number;
        
            constructor(name: string,defaultVolume : number) {
                this.store = [];
                this.name = name;
                this.defaultVolume = Utils.clampNumber(defaultVolume,[0,1]);
                this.volume = this.defaultVolume;
                United.Audio.Controller.controllerStore[this.name] = this;
            }
            
            linkSound(SoundAsset: Sup.Audio.SoundPlayer) : void {
                this.store.push(SoundAsset);
            }
        
            public static setVolume(name: string,volume: number) : boolean {
                if(Utils.has(United.Audio.Controller.controllerStore,name)) {
                    return United.Audio.Controller.controllerStore[name].setVolume(volume);
                }
                return false;
            }
            
            setVolume(volume: number) : boolean {
                this.volume = Utils.clampNumber(volume,[0,1]);
                this.store.forEach( (SoundAsset : Sup.Audio.SoundPlayer) => {
                    SoundAsset.setVolume(this.volume);
                });
                return true;
            }
        
            reset(store?:boolean) : void {
                this.setVolume(this.defaultVolume);
                if(store) {
                    this.store = [];
                }
            }
        
        }

        export class Core {
            
            private static store : { [key:string] : United.Audio.Playlist} = {};
            public static active : string;
            
            public static addPlaylist(name: string,playlist: United.Audio.Playlist) {
                this.store[name] = playlist;
            }
            
            public static setPlaylist(name: string,trackID?: number) : void {
                if(this.store[name]) {
                    if(this.active) {
                        this.store[this.active].stop();
                        this.store[name].start();
                        this.active = name;
                    }
                    else {
                        this.active = name;
                        this.store[name].start();
                    }
                }
            }

            public static next() {
                this.store[this.active].next();
            }

            public static prev() {
                this.store[this.active].prev();
            }

            public static update() {
                if(this.active)
                    this.store[this.active].update();
            }
            
        }

        export type trackOption = {
            asset : string,
            title? : string
            defaultVolume? : number
        }
        
        export interface playlistOption {
            defaultTrack?: number
            autoStart?: boolean
            startRandom?: boolean
        }
        
        export class Playlist {
        
            private activeTrack : number;
            public isPlaying : boolean = false;
            private soundInstance : Sup.Audio.SoundPlayer;
            private playlistSize : number;

            public startRandom : boolean = false;
            
            constructor(private tracksList : { [key:number] : United.Audio.trackOption },options?: playlistOption) {
                this.playlistSize = Utils.objectSize(this.tracksList);
                let track : number;
                options = options || {};
                if(options.defaultTrack && !options.startRandom) {
                    track = options.defaultTrack;
                }
                else if(options.startRandom) {
                    track = Utils.getRandom(0,this.playlistSize);
                    this.startRandom = true;
                }
                else {
                    track = 0;
                }
                this.setTrack(track);
                
                if(options.autoStart) {
                    this.start();
                }
            }

            public getTitle(ID?: number) : string {
                if(ID) {
                    if(Utils.has(this.tracksList,ID)) {
                        return this.tracksList[ID].title || "Undefined title!";
                    }
                    return "Undefined Track ID!";
                }
                return this.tracksList[this.activeTrack].title || "Undefined title!";
            }

            private setTrack(ID: number,start?:boolean) : void {
                const track : trackOption = this.tracksList[ID];
                this.activeTrack = ID;
                if(this.soundInstance) {
                    this.soundInstance.stop();
                }
                this.soundInstance = new Sup.Audio.SoundPlayer(track.asset,track.defaultVolume || 1.0,{loop: false});
                if(start) {
                    this.soundInstance.play();
                }
            }

            public start() : void {
                this.isPlaying = true;
                this.soundInstance.play();
            }
            
            public playTrack(trackID?: number) : void {
                if(trackID) {
                    this.setTrack(trackID);
                }
                else {
                    this.next();
                }
            }
            
            public stop() : void {
                this.isPlaying = false;
                this.soundInstance.pause();
            }

            public resume() : void {
                this.isPlaying = true;
                this.soundInstance.play();
            }
            
            public next() : void {
                let nextID : number = this.activeTrack + 1;
                if(!this.tracksList[nextID]) {
                    nextID = 0;
                }
                this.setTrack(nextID,true);
            }
            
            public prev() : void {
                let prevID : number = this.activeTrack - 1;
                if(!this.tracksList[prevID]) {
                    prevID = this.playlistSize;
                }
                this.setTrack(prevID,true);
            }
            
            public update() : void {
                if(this.isPlaying) {
                    if(this.soundInstance.getState() == Sup.Audio.SoundPlayer.State.Stopped) {
                        this.next(); // Prochaine piste!
                    }
                }
            }
            
        }
    
    }
    
}


