namespace United {
    
    export class langage {
        
        private static started : boolean = false; 
    
        public static active : string;
        public static langages : string[] = [];
        public static keyword : string = "lang_"
    
        protected static table = {};
    
        public static register(name: string,table: any) : void {
            if(!this.started) {
                this.started = true;
            }
            if(!Utils.has(this.table,name)) {
                if(!this.active)
                    this.active = name;
                this.langages.push(name);
                this.table[name] = table;
            }
        }
    
        public static get start() : boolean {
            return this.started;
        }
    
        public static add(...KV : string[]) : void {
            let i : number;
            for (i = 0; i < KV.length; i++)
                if(!this.has(KV[i][0])) 
                    this.table[this.active][KV[i][0]] = KV[i][1];
        }
    
        public static has(key: string,lang?: string) : boolean {
            return this.table[lang || this.active][key] ? true : false;
        }
    
        public static get(key: string,lang?: string) : string {
            if(this.has(key)) {
                return this.table[lang || this.active][key];
            }
            return "Lang | ERR";
        }
    
        public static update(key: string,new_value: string) : boolean {
            if(this.has(key)) {
                this.table[this.active][key] = new_value;
                return true;
            }
            return false;
        }
        
    }
    
}