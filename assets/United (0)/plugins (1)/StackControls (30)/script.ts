namespace United {
    /* --- Author ---
     * @Purexo
    /* --- ------ --- */
    export module StackControls {
        
        export interface ActionConstructor {
            keyboard?: string[];
            mButton?: number[];
            gamepad?: number[];
            gButton?: number[];
            gAxis?: number[];
            gTrigger?: number[];
        }

        /**
        Utilise la Sup.Input api
        */
        export class Action {
            keyboard : string[];
            mButton : number[];
            gamepad : number[];
            gButton : number[];
            gAxis : number[];
            gTrigger : number[];

            constructor(touches : ActionConstructor) {
                this.keyboard = touches.keyboard ? touches.keyboard : [];
                this.mButton = touches.mButton ? touches.mButton : [];
                this.gamepad = touches.gamepad ? touches.gamepad : [];
                this.gButton = touches.gButton ? touches.gButton : [];
                this.gAxis = touches.gAxis ? touches.gAxis : [];
                this.gTrigger = touches.gTrigger ? touches.gTrigger : [];
            }

            /**
              Renverra true si une des touches/boutons/clics est appuyé
              pour le gamepad : ça verifiera les gButton des gamepad
                pour chaque gamepad ça test gButton
            */
            isDown() : boolean {
                if (this.keyboard) {
                    const klength : number = this.keyboard.length;
                    for (let i : number = 0; i < klength; i++)
                        if (Sup.Input.isKeyDown(this.keyboard[i])) return true;
                }

                if (this.gamepad) {
                    const gplength : number = this.gamepad.length;
                    for (let i : number = 0; i < gplength; i++) {
                        if (this.gButton) {
                        let gblength = this.gButton.length;
                        for (let j = 0; j < gblength; j++)
                            if (Sup.Input.isGamepadButtonDown(this.gamepad[i], this.gButton[j])) return true;
                        }
                    }
                }

                if (this.mButton) {
                    const mlength : number = this.mButton.length;
                    for (let i : number = 0; i < mlength; i++)
                        if (Sup.Input.isMouseButtonDown(this.mButton[i])) return true;
                }

                return false;
            }

            /**
            * Renverra true si toutes les touches définies pour l'action sont préssés.
            */
            isAllDown() : boolean {
                let flag : { keyboard : boolean, gamepad : boolean , mouse : boolean } = {
                    keyboard: true,
                    gamepad: true,
                    mouse: true
                }

                if (this.keyboard) {
                    const klength : number = this.keyboard.length;
                    for (let i : number = 0; i < klength; i++)
                        if (!Sup.Input.isKeyDown(this.keyboard[i])) {
                            flag.keyboard = false;
                            break;
                        }
                }

                if (this.gamepad) {
                    const gplength : number = this.gamepad.length;
                    for (let i : number = 0; i < gplength; i++) {
                        if (this.gButton) {
                            const gblength : number = this.gButton.length;
                            for (let j = 0; j < gblength; j++)
                                if (!Sup.Input.isGamepadButtonDown(this.gamepad[i], this.gButton[j])) {
                                    flag.gamepad = false;
                                    break;
                                }
                        }
                    }
                }

                if (this.mButton) {
                    const mlength : number = this.mButton.length;
                    for (let i : number = 0; i < mlength; i++)
                        if (!Sup.Input.isMouseButtonDown(this.mButton[i])) {
                            flag.mouse = false;
                            break;
                        }
                }

                return flag.mouse && flag.keyboard && flag.gamepad;
            }

            /**
              Renverra true si une des touches/boutons/clics viens d'être pressé
              pour le gamepad : ça verifiera les gButton des gamepad
                pour chaque gamepad ça test gButton

              @options : { autoRepeat?: boolean; } ne seras testé qu'avec le clavier
            */
            wasJustPressed(options?: { autoRepeat?: boolean; }) : boolean {
                if (this.keyboard) {
                    const klength : number = this.keyboard.length;
                    for (let i : number = 0; i < klength; i++)
                        if (Sup.Input.wasKeyJustPressed(this.keyboard[i], options)) return true;
                }

                if (this.gamepad) {
                    const gplength : number = this.gamepad.length;
                    for (let i : number = 0; i < gplength; i++) {
                        if (this.gButton) {
                            const gblength : number = this.gButton.length;
                            for (let j = 0; j < gblength; j++)
                                if (Sup.Input.wasGamepadButtonJustPressed(this.gamepad[i], this.gButton[j])) return true;
                        }
                    }
                }

                if (this.mButton) {
                    const mlength : number = this.mButton.length;
                    for (let i : number = 0; i < mlength; i++)
                        if (Sup.Input.wasMouseButtonJustPressed(this.mButton[i])) return true;
                }

                return false;
            }

            /**
              Renverra true si une des touches/boutons/clics viens d'être laché
              pour le gamepad : ça verifiera les gButton des gamepad
                pour chaque gamepad ça test gButton
            */
            wasJustReleased() : boolean {
                if (this.keyboard) {
                    const klength : number = this.keyboard.length;
                    for (let i = 0; i < klength; i++)
                        if (Sup.Input.wasKeyJustReleased(this.keyboard[i])) return true;
                }

                if (this.gamepad) {
                    const gplength : number = this.gamepad.length;
                    for (let i = 0; i < gplength; i++) {
                        if (this.gButton) {
                            const gblength : number = this.gButton.length;
                            for (let j = 0; j < gblength; j++)
                                if (Sup.Input.wasGamepadButtonJustReleased(this.gamepad[i], this.gButton[j])) return true;
                        }
                    }
                }

                if (this.mButton) {
                    const mlength : number = this.mButton.length;
                    for (let i = 0; i < mlength; i++)
                        if (Sup.Input.wasMouseButtonJustReleased(this.mButton[i])) return true;
                }

                return false;
            }


            /**
                Retournera la valeurs des axes saisies dans gAxis
                [
                gamepad1 : [
                  axis1 : valeur1,
                  axis2 : valeur2
                ], 
                gamepad2 : [
                  axis1 : valeur3,
                  axis2 : valeur4
                ]
                ]

                ret[0][0] => valeur1
                ret[0][1] => valeur2
                etc...
                si pas de valeurs trouvé : return undefined
            */
            getAxisValues() : number[][] {
                if (this.gamepad) {
                    const ret : any[] = []
                    const gplength : number = this.gamepad.length;
                    for (let i : number = 0; i < gplength; i++) {
                        ret[i] = [];
                        if (this.gButton) {
                            const galength : number = this.gAxis.length;
                            for (let j : number = 0; j < galength; j++)
                                ret[i][j] = Sup.Input.getGamepadAxisValue(this.gamepad[i], this.gAxis[j]);
                        }
                    }
                    return ret;
                }
                return undefined;
            }

            /**
                Retournera la valeurs des trigers saisies dans gTrigger
                [
                gamepad0 : [
                  trigger0 : valeur0,
                  trigger1 : valeur1
                ], 
                gamepad0 : [
                  trigger0 : valeur2,
                  trigger1 : valeur3
                ]
                ]

                ret[0][0] => valeur0
                ret[0][1] => valeur1
                etc...
                si pas de valeurs trouvé : return undefined
            */
            getTriggersValues() : any {
                if (this.gamepad) {
                    const ret : any[] = []
                    const gplength : number = this.gamepad.length;
                    for (let i : number = 0; i < gplength; i++) {
                        ret[i] = [];
                        if (this.gTrigger) {
                            const gtlength : number = this.gTrigger.length;
                            for (let j : number = 0; j < gtlength; j++)
                                ret[i][j] = Sup.Input.getGamepadAxisValue(this.gamepad[i], this.gTrigger[j]);
                        }
                    }
                    return ret;
                }
                return undefined;
            }

            /**
              Permet d'ajouter une touche à un control:
              @action : "UP" ou "BOTTOM" etc... : le tableau de control à éditer
              @periphType : keyboard ... cf ActionConstructor
              @newKey : "UP" par exemple (cf Sup.Input Valid key names)

              examples : 
                .addKey("keyboard", "UP")
                ajoute la possibilité d'utiliser la touche UP (flèche) pour utiliser le control UP
            */
            addKey(periphType : string, newKey : string | number) {
                this[periphType].push(newKey);
            }

            /**
              Permet d'éditer une touche de control:
              @periphType : keyboard ... cf ActionConstructor
              @oldKey : "Z" par exemple (based on window.KeyEvent.DOM_VK_*)
              @newKey : "UP" par exemple (cf Sup.Input Valid key names)

              examples : 
                .editKey("keyboard", "Z", "UP")
                remplace la possibilité d'utiliser la touche Z par UP (flèche) pour utiliser le control UP
            */
            editKey(periphType : string, oldKey : string | number, newKey : string | number) {
                const indexKey : number = this[periphType].indexOf(oldKey);
                this[periphType][indexKey] = newKey;
            }

            /**
              Permet de suprimer une touche utilisé par un control:
              @periphType : keyboard ... cf ActionConstructor
              @oldKey : "UP" par exemple (cf Sup.Input Valid key names)

              examples : 
                .removeKey("keyboard", "W")
                suprime la possibilité d'utiliser la touche W pour utiliser le control UP
            */
            removeKey(periphType : string, oldKey : string | number) {
                let indexKey = this[periphType].indexOf(oldKey);
                delete this[periphType][indexKey];
            }
    }

        /**
            Ensemble d'action utilisable

            Utilisation :
              1. creez votre propre classe en suivant l'exemple ControlP1
              2. control = new MyControl()

            if (control.ACTION.isDown()) {
              //make your things
            }

            /!\ ----------------------------------------------------
            /!\ Tous les noms de vos actions seront mis en majuscules
            /!\ toutes les fonctions de cette classe mets les nom d'action en majuscule
            /!\
            /!\ control["up"] ne fonctionnera pas même su vous avez fait
            /!\ up: new Action({keyboard: ["Z", "W", "UP"]}),
        */
        export abstract class Controls {

            constructor (ctrl : { [key:string] : Action }) {
                for (const key in ctrl) {
                    this[key.toUpperCase()] = ctrl[key];
                }
            }

            /* --- --- --- 
             Keys
            --- --- --- */

            /**
                Permet d'ajouter une touche à un control:
                @actionName : "UP" ou "BOTTOM" etc... : le tableau de control à éditer
                @periphType : keyboard ... cf ActionConstructor
                @newKey : "UP" par exemple (cf Sup.Input Valid key names)

                examples : 
                .addKey("UP", "keyboard", "UP")
                ajoute la possibilité d'utiliser la touche UP (flèche) pour utiliser le control UP
            */
            addKey(actionName : string, periphType : string, newKey : string | number) {
                this[actionName.toUpperCase()].addKey(periphType, newKey);
            }

            /**
                Permet d'éditer une touche de control:
                @actionName : "UP" ou "BOTTOM" etc... : le tableau de control à éditer
                @periphType : keyboard ... cf ActionConstructor
                @oldKey : "Z" par exemple (based on window.KeyEvent.DOM_VK_*)
                @newKey : "UP" par exemple (cf Sup.Input Valid key names)

                examples : 
                .editKey("UP", "keyboard", "Z", "UP")
                remplace la possibilité d'utiliser la touche Z par UP (flèche) pour utiliser le control UP
            */
            editKey(actionName : string, periphType : string, oldKey : string | number, newKey : string | number) {
                this[actionName.toUpperCase()].editKey(periphType, oldKey, newKey);
            }

            /**
                Permet de suprimer une touche utilisé par un control:
                @actionName : "UP" ou "BOTTOM" etc... : le tableau de control à éditer
                @periphType : keyboard ... cf ActionConstructor
                @oldKey : "UP" par exemple (cf Sup.Input Valid key names)

                examples : 
                .removeKey("UP", "keyboard", "W")
                suprime la possibilité d'utiliser la touche W pour utiliser le control UP
            */
            removeKey(actionName : string, periphType : string, oldKey : string | number) {
                this[actionName.toUpperCase()].removeKey(periphType, oldKey);
            }

            /* --- --- ---
            Actions
            --- --- --- */

            /**
                Permet d'ajouter une Action :
                @newAction : nom de la nouvelle Action à ajouter
                @action : action à assigner

                examples : 
                .addAction("INTERRACT", ["E", "F"])
                rajoutera le control INTERRACT utilisable avec les touches E et F
            */
            addAction(actionName : string, action : Action) {
                this[actionName.toUpperCase()] = action;
            }

            /**
                Permet d'éditer complétement un control déjà assigné
                @control : nom du control à modifier
                @tabKey : tableau de touches à assigner au control

                examples : 
                .editControl("UP", ["I", "UP"])
                modifiera le control UP pour le rendre utilisable avec les touches I et UP (fleche)
            */
            editAction(actionName : string, action : string[]) {
                this[actionName.toUpperCase()] = action;
            }

            /**
                Permet de renommer un control
                @oldName : ancien nom de control
                @newName : nouveau nom de control

                examples : 
                .renameControl("UP", "MONTER")
                rennomerra le control UP en MONTER
            */
            renameAction(oldName : string, newName : string) {
                oldName = oldName.toUpperCase();
                this[newName.toUpperCase()] = this[oldName];
                delete this[oldName];
            }

            /**
                Permet de suprimer un control assigné
                @control : nom du control à suprimer

                examples : 
                .editControl("INTERRACT")
                Supprimera des control utilisable le control INTERRACT
            */
            removeAction(actionName : string) {
                delete this[actionName.toUpperCase()];
            }

        }
    
    }
}