namespace United {
    
    export interface AnimationConstructor {
        actor?: string;
        startPosition?: Sup.Math.Vector3;
        firstStep?: number;
        loop?: boolean;
        animationStep: { [key:number] : AnimationStep }
    }

    export interface AnimationStep {
        frame: number;
        next?: number;
        pattern: United.AnimationPattern | United.AnimationPattern[];
    }
    
    // TODO : Amélioration des frames
    // TODO : Extension pour l'animation des textes! 
    // TODO : Plus de pattern

    export class Animation {

        public actor: Sup.Actor;
        private animationStep : { [key:number] : AnimationStep } = {};
        private activeStepID : number;
        private animationState : boolean = true;
        private animationLoop : boolean;

        constructor(opt: AnimationConstructor) {
            this.actor = Sup.getActor(opt.actor);
            this.animationLoop = opt.loop || false;
            
            // Create actor
            if(!this.actor) 
                this.actor = new Sup.Actor(opt.actor);
            
            if(opt.startPosition) {
                this.actor.setPosition(opt.startPosition);
            }
            
            // Define default Animation Step!
            this.animationStep = opt.animationStep;
            if(!this.initStep(opt.firstStep || 0)) {
                this.animationState = false;
            }
        }

        public initStep(stepID) : boolean {
            if(this.animationStep[stepID]) {
                this.activeStepID = stepID;
                const Step : United.AnimationStep = this.animationStep[this.activeStepID];
                if(Step.pattern instanceof Array) {
                    const patternArray : United.AnimationPattern[] = <United.AnimationPattern[]>Step.pattern;
                    patternArray.forEach( (pattern: United.AnimationPattern) => {
                        pattern.init(this.actor,this.animationStep[this.activeStepID].frame);
                    });
                }
                else {
                    const pattern : United.AnimationPattern = <United.AnimationPattern>Step.pattern;
                    pattern.init(this.actor,this.animationStep[this.activeStepID].frame);
                }
                return true;
            }
            return false;
        }

        public restart() {
            this.initStep(0);
            this.animationState = true;
        }

        public update() : void {
            if(this.animationState) {
                // On récupère le pattern en cours
                const Step : United.AnimationStep = this.animationStep[this.activeStepID];
                let goNext : boolean = false;
                
                if(Step.pattern instanceof Array) {
                    
                    const patternArray : United.AnimationPattern[] = <United.AnimationPattern[]>Step.pattern;
                    let allNext : boolean = true;
                    patternArray.forEach( (pattern: United.AnimationPattern) => {
                        if(!pattern.update()) {
                            allNext = false;
                        }
                    });
                    
                    if(allNext) {
                        goNext = true;
                    }
                    
                }
                else {
                    // On update le pattern!
                    const Pattern : United.AnimationPattern = <United.AnimationPattern>Step.pattern;
                    if(Pattern.update()) {
                        goNext = true;
                    }
                }
                
                if(goNext) {
                    if(!Step.next) {
                        if(this.animationStep[this.activeStepID + 1]) {
                            if(!this.initStep(this.activeStepID + 1)) {
                                this.animationState = false;
                            }
                        }
                        else {
                            if(this.animationLoop) {
                                this.initStep(0);
                            }
                            else {
                                this.animationState = false;
                            }
                        }
                    }
                    else {
                        if(!this.initStep(Step.next)) {
                            this.animationState = false;
                        }
                    }
                }
            }
        }

    }

    export abstract class AnimationPattern {
        
        public frame: number;
        public timer: United.dt;
        public actor: Sup.Actor;

        constructor() {}

        public init(actor:Sup.Actor,frame: number) : void {
            this.actor = actor;
            this.frame = frame;
            this.timer = new United.dt({dt:this.frame});
            if(this.configure) {
                this.configure();
            }
        }

        public reset() : void {
            this.timer.reset();
        }

        public walk() : boolean {
            return this.timer.walk() ? true : false;
        }

        abstract configure(): void;

        abstract update() : boolean;

    }

    export module Pattern {

        export class Blink extends United.AnimationPattern {

            constructor(public blinkInterval: number) {
                super();
            }
            
            configure() : void {
                Sup.log(`configure blink in intervalOf=>${this.blinkInterval}`);
                this.actor.textRenderer.setOpacity(1.0);
            }
            
            update() : boolean {
                const state : boolean = super.walk(); 
                this.actor.textRenderer.setOpacity( (this.timer.tick % this.blinkInterval || state) ? 1 : 0 );
                return state;
            }

        }

        export class scaleBump extends United.AnimationPattern {
            
            public defaultScale : Sup.Math.Vector3;

            constructor(public scaleOffset: Sup.Math.Vector3,public bumpInterval: number) {
                super();
            }
            
            configure() : void {
                this.defaultScale = this.actor.getLocalScale();
                Sup.log(`configure scaleBumping with scale offsetOf=>${this.scaleOffset}`);
                //this.actor.setLocalScale(this.defaultScale);
            }
            
            update() : boolean {
                const state : boolean = super.walk(); 
                this.actor.setLocalScale( (this.timer.tick % this.bumpInterval || state) ? this.defaultScale : this.defaultScale.clone().add(this.scaleOffset) );
                return state;
            }

        }

        export class Fade extends United.AnimationPattern {

            constructor(private direction: boolean) {
                super();
            }
            
            configure() : void {
                Sup.log(`configure fade in direction=>${this.direction}!`);
                this.actor.textRenderer.setOpacity( this.direction ? 0 : 1 );
            }
            
            update() : boolean {
                const state : boolean = super.walk(); 
                const factor : number = this.timer.tick / this.frame;
                const progression : number = (1 - (1 - factor) * (1 - factor));
                if(state) {
                    this.actor.textRenderer.setOpacity( this.direction ? 1 : 0 );
                }
                else {
                    this.actor.textRenderer.setOpacity( this.direction ? progression : 1 - progression );
                }
                return state;
            }

        }
                   
    }
    
}
