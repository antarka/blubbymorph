namespace United {
    
    export enum Animation {
        show = 1,
        hide = 2
    }
    
    export interface Animation_Options {
        speed : number
    }
    
    class ActorAnimation {
        
        constructor(Animation : Animation,opts : Animation_Options) {
            
        }
        
    }
    
}