class U_Core extends Sup.Behavior {
    
    protected name : string; 
    protected game : United.game;

    awake() {
        this.game = new United.game(this.name);
        this.awakening();
    }
    
    protected awakening() {}
    protected main() {}
    protected each() {}

    update() {
        if(this.game.state) {
            this.main();
            this.game.update();
        }
    }
}
Sup.registerBehavior(U_Core);
