class DialogueBossBehavior extends United.AreaBehavior {

    configure($) {
        this.target = $.player.actor;
        this.unique_in = true;
        this.area_length = 1;
    }
    
    in($) {
        $.winDialogue.setDialogue(
            [
                "Blubby ! If you kill the boss\n you will gain the power of metamorphose!",
                "Use the right click of your\n mouse when you are next a mob!"
            ]
        );
    }
    
}
Sup.registerBehavior(DialogueBossBehavior);
