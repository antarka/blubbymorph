namespace Utils {
    export function generateRandomPosition(center:Sup.Math.XY, range:number) {
        let x = center.x + Sup.Math.Random.float(-range, range);
        let y = center.y + Sup.Math.Random.float(-range, range);
        return new Sup.Math.Vector2(x, y);
    }
}