class UIInput extends UIButton {
  
  // Behavior & Actions property
  public isFocus : boolean = false; 

  // Input property
  public placeholder : string = "";
  public value : string = "";

  awake() {
    United.input.actions.forEach((action : string) => {
      this.actor[action] = undefined;
    });
  }
  
  protected focus() {}
  protected unfocus() {}
  protected enter() {}

  update() {
    this.process();
    if(United.UIRay.intersectActor(this.actor,false).length > 0) {
      
      // Hover action
      if(!this.isHover) {
        this.isHover = true;
        this.call("hover");
      }
      
      // LeftClick action
      if(Sup.Input.wasMouseButtonJustPressed(0)) {
        
        // Focus Action
        if(!this.isFocus) {
          this.isFocus = true;
          this.call("focus");
        }
        
        // Left click Action
        this.call("click");
      }
      
      // Enter action
      if(this.isFocus) {
        
        if(Sup.Input.wasKeyJustPressed("ENTER")) {
          this.call("enter");
        }
        
        // Input interaction here ! 
        
      }
    }
    else { 
      
      // Hover action 
      if(this.isHover) {
        this.isHover = false;
        this.call("unhover");
      }
      
      // Unfocus action 
      if(this.isFocus) {
        this.isFocus = false;
        this.call("unfocus");
      }
      
    } 
  }
  
}
Sup.registerBehavior(UIInput);
