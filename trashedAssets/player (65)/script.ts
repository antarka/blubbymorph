class PlayerBehavior extends United.Behavior {
    
    private controls : playerSampleControl = new playerSampleControl();
    
    awake() {
        Game.declare('playerActor',this.actor);
        Game.declare('playerSpeed',0.004);
        Game.declare('playerJumpSpeed',0.15);
    }

    main($) {
        Sup.ArcadePhysics2D.collides(this.actor.arcadeBody2D,Sup.ArcadePhysics2D.getAllBodies());
        const Velocity : Sup.Math.Vector2 = this.actor.arcadeBody2D.getVelocity();
        
        if(this.controls.RIGHT.isDown()) {
            Velocity.x += $.playerSpeed;
        }
        else if(this.controls.LEFT.isDown()) {
            Velocity.x -= $.playerSpeed;
        }
        else {
            Velocity.x = 0;
        }
        
        if(this.controls.UP.wasJustPressed()) {
            Velocity.y += $.playerJumpSpeed;
        }
        
        this.actor.arcadeBody2D.setVelocity(Velocity);
    }
}
Sup.registerBehavior(PlayerBehavior);
