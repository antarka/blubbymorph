class ExplosionBehavior extends Sup.Behavior {
    awake() {
        
    }

    update() {
        if (this.actor.spriteRenderer.getAnimationFrameIndex() >= this.actor.spriteRenderer.getAnimationFrameCount() - 1)
            this.actor.destroy();
    }
}
Sup.registerBehavior(ExplosionBehavior);
