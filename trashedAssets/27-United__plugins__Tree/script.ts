namespace United {
    
    export module tree {
        
        export function exist(path: string) : Sup.Asset {
            try {
                let Asset : Sup.Asset = Sup.get(path);
                return Asset;
            }
            catch(Err) {
                return null;
            }
        }
        
        /*export function is(path: string,type: any) {
            if(United.tree.exist(path)) {
                let Asset : Sup.Asset = Sup.get(path);
                if(Asset instanceof type) {
                    return true;
                }
            }
            return false;
        } */
    }

}