class ChestBehavior extends United.AreaBehavior {
    
    public area : number = 2;
    
    configure() {
        this.target = Sup.getActor("Player");
        this.area_length = this.area;
    }
    
    in() {
        Sup.log("player in chest Area!");
    }
}
Sup.registerBehavior(ChestBehavior);
